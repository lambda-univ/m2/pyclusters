from pyclusters.core.logger import LOGGER_NO_LOG, LOGGER_DEBUG_LEVEL, LoggerFactory
from argparse import ArgumentParser
from pyclusters.core.starter import PyclusterApplication
from os import getcwd, remove
from os.path import join, abspath, exists, isabs
import pyclusters
from inspect import getfile
import regex as re
from ntpath import basename
from shutil import copyfile
from pathlib import Path


def current_path(): return getcwd()
def lib_name(): return pyclusters.__name__
def lib_path(): return pyclusters.__path__[0]
def resources_path(): abspath(join(lib_path(), "", "../resources"))

def create_parser(start_callback, test_callback, listmod_callback, pushmod_callback, pullmod_callback, delmod_callback):
    parser = ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true", default=False)
    subparsers = parser.add_subparsers(help="Sub-Parser help", dest="command")

    # start command: start PyClusters application
    start = subparsers.add_parser("start", help="Start PyClusters application.")
    start.set_defaults(func=start_callback)

    # listmod command: list all modules from app
    listmod = subparsers.add_parser("listmod", help="List all modules registered in app.")
    listmod.set_defaults(func=listmod_callback)
    listmod.add_argument("-t", "--type",
                         help="""Displays all module which inherit from module type only when type match with given pattern.
                         For example, to display all modules inherited from IO types, specify 'IO' as pattern. 
                         """
    )
    listmod.add_argument("-m", "--module", help="Displays module when it match with given pattern")

    # testmod command: test module integrity
    testmod = subparsers.add_parser("testmod", help="Test module to check integrity")
    testmod.set_defaults(func=test_callback)

    # pushmod command: push a module in app
    pushmod = subparsers.add_parser( "pushmod", help="Push given module file in PyClusters" )
    pushmod.set_defaults(func=pushmod_callback)
    pushmod.add_argument("module", help="Specify the module push")
    pushmod.add_argument(
        "-f", "--force", action="store_true", help="Force push module even if a file with same name already exists"
    )
    pushmod.add_argument(
        "-l",
        "--location",
        help="""Specify module destination. 
        For example, /path/to/module.py will locate module at {pyclusters_location}/modules/path/to/module.py
        """
    )


    # pullmod command: pull module form app to a current or given location
    pullmod = subparsers.add_parser("pullmod", help="Pull module from PyClusters to current or given location")
    pullmod.set_defaults(func=pullmod_callback)
    pullmod.add_argument("module", help="Specify the module pull")
    pullmod.add_argument(
        "-f", "--force", action="store_true", help="Force push module even if a file with same name already exists"
    )
    pullmod.add_argument(
        "-o", "--filename" ,help="""Specify module filename."""
    )

    # delmod command: Delete module from position
    delmod = subparsers.add_parser( "delmod", help="Delete module." )
    delmod.set_defaults(func=delmod_callback)
    delmod.add_argument("module", help="Module to be removed")

    # test command: run test though module to check modules integrity
    testmod = subparsers.add_parser("test", help="Launch PyClusters test")
    testmod.set_defaults(func=test_callback)

    return parser

def start( args ):
    verbosity_level = LOGGER_DEBUG_LEVEL if args.verbose else LOGGER_NO_LOG

    # Start application
    starter = PyclusterApplication(
        lib_name=lib_name(),
        lib_path=lib_path()
    )
    starter.run()

from pyclusters.test.pyclusters import launch_test

def test( args ):
    launch_test()


def listmod(args):
    # creating module register
    logger = LoggerFactory.build_logger(LOGGER_NO_LOG)
    pyclusters = PyclusterApplication(lib_name(), lib_path())
    modules_register = pyclusters.module_register()

    # define module and module pattern patterns
    type_pattern = args.type if args.type else ""
    module_pattern = args.module if args.module else ""

    # search modules
    displayed_modules = {}
    for module_type in modules_register.types:
        # displays type only when match
        module_type_name = module_type.__name__
        if not re.search(type_pattern, module_type_name):
            continue

        for module in modules_register.find_modules_of_class(module_type):
            # displays module only when match specified pattern
            module_name = module.__name__
            if not re.search(module_pattern, module_name):
                continue

            # module and type match, so insert them in displayed modules in order to display it later
            if module_type_name not in displayed_modules:
                displayed_modules[module_type_name] = []

            displayed_modules[module_type_name].append( ( module_name, module ) )


    # listing module
    modules_location = pyclusters.modules_path()
    for module_type_name, modules in displayed_modules.items():
        print("Type {}:".format(module_type_name))
        for module_name, module in modules:
            print("\t- {} on {}".format(module_name, getfile(module).replace(modules_location, "")))

def push_module( args ):

    # define module location
    # stop if module doesn't exists
    module_file_name = args.module
    module_file_location = join(getcwd(), args.module)
    if not exists(module_file_location):
        print("File {} doesn't exist".format(module_file_name))
        exit()

    # define module file's destination
    # module file's destination must be an absolute path with a chroot to modules location
    # so module located at {{pyclusters_modules}}/path/to/module.py can be simplified as /path/to/modules.py
    application = PyclusterApplication( lib_name(), lib_path() )
    module_file_destination = args.location
    if not isabs(module_file_destination):
        print("Module file's destination must be an absolute path relative from modules location, ex: /io/textual.py")
        exit()
    module_destination = application.modules_path() + module_file_destination

    # prevent module overwriting
    if exists(module_destination) and not args.force:
        print("Pushed aborted: A module with the same name already exists")
        exit()

    # before copying module file,
    module_destination_path = Path(module_destination.replace(basename(module_destination), ""))
    if not module_destination_path.exists():
        module_destination_path.mkdir()
    print("Copy module from {} to {}".format(module_file_location, module_destination)  )
    copyfile( module_file_location, module_destination )



def pull_module( args ):
    # define module location
    application = PyclusterApplication( lib_name(), lib_path() )
    module_path = application.modules_path() + args.module

    # can only pull existing module
    if not exists(module_path):
        print("No module exists at location {}".format(args.module))
        exit(1)

    # if destination not specified, define it as the module name
    module_filename = basename(module_path) if not args.filename else args.filename
    module_destination = join(getcwd(), module_filename)

    # prevent file overwriting when destination already exists
    if exists( module_destination ) and not args.force:
        print("Pull aborted: A file with the same already exists ")
    else:
        print("Pulling module")
        copyfile( module_path, module_destination )

def delete_module( args ):
    # ensures that delete module is absolute path
    if not isabs(args.module):
        print("Module file is not absolute. Please specify module as /io/textual.py")
        exit()

    # define module location
    application = PyclusterApplication(lib_name(), lib_path())
    module_path = application.modules_path() + args.module

    # can only delete existing module
    if not exists(module_path):
        print("No module exists at location {}".format(args.module))
        exit(1)

    # confirm module deletion
    response = input("Module file {} will be deleted. Are you sure ? (Y/n) ".format(args.module))
    if response != 'Y':
        exit()

    # delete module
    remove(module_path)




if __name__ == '__main__':
    # parse argument
    parser = create_parser(
        start_callback=start,
        test_callback=test,
        listmod_callback=listmod,
        pullmod_callback=pull_module,
        pushmod_callback=push_module,
        delmod_callback=delete_module,
    )
    args = parser.parse_args()
    if args.command:
        args.func(args)
    else:
        parser.print_help()




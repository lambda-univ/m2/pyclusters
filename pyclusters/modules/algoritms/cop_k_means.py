from pyclusters.core.entities import Database, Constraints, Labels, LabelsFactory
from pyclusters.core.interface import ClusteringAlgorithmModule, ParametersBag, Range
from pyclusters.exceptions.interface import ModuleExecutionError
from copkmeans.cop_kmeans import cop_kmeans



class COPKMeansClusteringAlgorithmModule(ClusteringAlgorithmModule):
    """COP-KMeans clustering algorithm module.

    This module uses COP-Kmeans Behrouz-Babaki's implementation available on github.
    """
    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add("k", Range.build( min=1 ))
        return parameters

    def predict(self, database: Database, constraints: Constraints, parameters) -> Labels:
        # computes clusters
        # Warning: if clustering cannot be found due to constraints insatiability, clusters will be None
        clusters, center = cop_kmeans(
            dataset=database.get_data().tolist(),
            k=int(parameters["k"]),
            ml=constraints.get_ml().tolist(),
            cl=constraints.get_cl().tolist()
        )

        # So if no clustering found, raise appropriate exception
        if clusters is None:
            raise ModuleExecutionError(
                title="Clustering not found",
                message="A clustering cannot be found"
            )


        return LabelsFactory.build_from_data( labels=clusters )
from pyclusters.core.entities import Labels
from pyclusters.core.interface import ComparatorModule, OutputStream, ParametersBag, Choices
from sklearn.metrics import precision_score, recall_score


class PrecisionRecallComparatorModule(ComparatorModule):
    """Precision and Recall comparator module.

    average : {'micro', 'macro', 'samples', 'weighted', 'binary'} default='binary'
    """

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add("Average", Choices.build([ "micro", "macro", "samples", "binary" ], default_index=0))
        return parameters


    def compare(self, printer: OutputStream, first_labels: Labels, second_labels: Labels, parameters):
        average = parameters["Average"]

        precision = precision_score( first_labels.get_labels(), second_labels.get_labels(), average=average )
        printer.println("Precision: {}".format(precision))

        recal = recall_score(first_labels.get_labels(), second_labels.get_labels(), average=average)
        printer.println("Recall: {}".format(recal))
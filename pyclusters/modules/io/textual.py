from pyclusters.core.entities import Labels, ConstraintsEntity, Database, Database, ConstraintsFactory, \
    Constraints
from pyclusters.core.entities import LabelsFactory, DatabaseFactory
from pyclusters.core.interface import LabelsIOModule, ConstraintsIOModule, DatabaseIOModule
from pyclusters.exceptions.core import InvalidFileFormatError



class TextualOneByLineDatabaseIOModule(DatabaseIOModule):
    """Reads a database in a textual format with one instance by line.
    The first line is always ignored because it's the instances number.
    """
    def write(self, filename: str, database: Database):
        with open(filename, "w") as f:
            for instance in database.get_data():
                line = " ".join([ str(x) for x in instance ])
                f.write(line + "\n")


    def read(self, filename: str) -> Database:
        data = []
        with open(filename, 'r') as f:
            lines = f.readlines()[1:]
            # if line is redces
            for line in lines:
                # read each instance in one line
                line = line.strip()
                if line != '':
                    data_string = line.split()
                    d = [float(i.strip()) for i in data_string if i != '']
                    data.append(d)
        return DatabaseFactory.build_from_data( data )

class TextualOneByLineLabelsIOModule(LabelsIOModule):
    """Reads and writes label from textual format with one label by line."""
    def write(self, filename: str, labels: Labels):
        with open(filename, 'r') as f:
            for label in labels:
                f.write( "{}\n".format(label) )

    def read(self, filename: str):
        labels = []
        with open(filename, 'r') as f:
            for line in f:
                line = line.strip().replace("\n", "")
                if line != '':
                    labels.append(float(line))
        return LabelsFactory.build_from_data(labels=labels)

class TextualOneByLineConstraintsIOModule(ConstraintsIOModule):
    MUST_LINK_CONSTRAINT = 1
    CANNOT_LINK_CONSTRAINT = -1

    def write(self, filename: str, constraints: Constraints):
        pass

    def read(self, filename: str) -> Constraints:
        ml = []
        cl = []

        with open(filename, 'r') as f:
            for line in f:
                constraint = [int(n) for n in line.split()]
                x, y, kind = constraint
                if kind == TextualOneByLineConstraintsIOModule.MUST_LINK_CONSTRAINT:
                    ml.append([x, y])
                elif kind == TextualOneByLineConstraintsIOModule.CANNOT_LINK_CONSTRAINT:
                    cl.append([x, y])
                else:
                    raise InvalidFileFormatError("Invalid constraints type")


        return ConstraintsFactory.build_from_data(
            ml = ml,
            cl = cl
        )
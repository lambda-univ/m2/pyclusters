
from matplotlib.figure import Figure
from pyclusters.core.entities import Labels, Database
from pyclusters.core.interface import ClusteringVisualizerModule, DatabaseVisualizerModule, ParametersBag, Range, \
    Choices, Confirmation, Entry


class SelectableColumnsVisualizerModule(DatabaseVisualizerModule, ClusteringVisualizerModule):
    """Displays coloumns with specified cm
    """

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add_range( "Column 1", Range.build( min = 1 ) )
        parameters.add_range( "Column 2", Range.build( min = 2 ) )
        return parameters

    def visualize_database(self, database: Database, parameters) -> Figure:
        colonne1 = int(parameters["Column 1"])
        colonne2 = int(parameters["Column 2"])

        data = database.get_data()

        f = Figure()
        a = f.add_subplot(111)
        a.scatter(data.transpose()[colonne1], data.transpose()[colonne2])

        return f

    def visualize_clustering(self, database: Database, labels: Labels, parameters) -> Figure:
        colonne1 = int(parameters["Column 1"])
        colonne2 = int(parameters["Column 2"])

        data = database.get_data()

        f = Figure()
        a = f.add_subplot(111)
        a.scatter(data.transpose()[colonne1], data.transpose()[colonne2], c=labels.get_labels())

        return f



class TestParametersVisualizerModule(DatabaseVisualizerModule, ClusteringVisualizerModule):
    """
    Simple and Stupid simple visualizer.
    Only scatter first and second database's columns.
    Enjoy !
    """

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add( "Range", Range.build( min = 1 ) )
        parameters.add("Choices", Choices.build(["A", "B", "C"]))
        parameters.add("Confirmation", Confirmation.build(default=True))
        parameters.add("Entry", Entry.build(default="Coucou !"))
        return parameters

    def visualize_database(self, database: Database, parameters) -> Figure:
        colonne1 = int(parameters["Colonne 1"])
        colonne2 = int(parameters["Colonne 2"])

        data = database.get_data()

        f = Figure()
        a = f.add_subplot(111)
        a.scatter(data.transpose()[colonne1], data.transpose()[colonne2])

        return f

    def visualize_clustering(self, data: Database, labels_set: Labels, parameters) -> Figure:
        return self.visualize_database(data, parameters)

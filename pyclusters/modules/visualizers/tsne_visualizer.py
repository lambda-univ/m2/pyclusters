
from matplotlib.figure import Figure
from pyclusters.core.entities import Labels, Database
from pyclusters.core.interface import ClusteringVisualizerModule, DatabaseVisualizerModule, ParametersBag, Range
from sklearn.manifold import TSNE


class TSNEVisualizerModule(DatabaseVisualizerModule, ClusteringVisualizerModule):
    """
    t-distributed Stochastic Neighbor Embedding.

    t-SNE [1] is a tool to visualize high-dimensional data.
    It converts similarities between data points to joint probabilities and tries to minimize the Kullback-Leibler
    divergence between the joint probabilities of the low-dimensional embedding and the high-dimensional data.
    t-SNE has a cost function that is not convex, i.e. with different initializations we can get different results.
    """

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add_range( "learning rate", Range.build( min = 10, max = 1000, default=200 ) )
        parameters.add_range( "Nb iterations", Range.build(min=250, default=1000) )
        return parameters


    def visualize_database(self, database: Database, parameters) -> Figure:
        learning_rate = parameters["learning rate"]
        n_iter = int(parameters["Nb iterations"])

        data = database.get_data()
        data_embedded = TSNE(
            n_components=2,
            learning_rate=learning_rate,
            n_iter=n_iter
        ).fit_transform(data)
        data_embedded_transpose = data_embedded.transpose()


        f = Figure()
        a = f.add_subplot(111)
        a.scatter(data_embedded_transpose[0], data_embedded_transpose[1])

        return f

    def visualize_clustering(self, database: Database, labels: Labels, parameters) -> Figure:
        learning_rate = parameters["learning rate"]
        n_iter = int(parameters["Nb iterations"])

        data = database.get_data()
        data_embedded = TSNE(
            n_components=2,
            learning_rate=learning_rate,
            n_iter=n_iter
        ).fit_transform(data)
        data_embedded_transpose = data_embedded.transpose()

        f = Figure()
        ax = f.add_subplot(111)
        ax.scatter(data_embedded_transpose[0], data_embedded_transpose[1], c=labels.get_labels())

        return f
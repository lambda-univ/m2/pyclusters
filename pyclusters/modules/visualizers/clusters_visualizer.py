from matplotlib.figure import Figure

from pyclusters.core.entities import Database, Labels
from pyclusters.core.interface import ClusteringVisualizerModule, DatabaseVisualizerModule
from pyclusters.exceptions.interface import ModuleExecutionError


class TwoDimensionalVisualizerModule(ClusteringVisualizerModule, DatabaseVisualizerModule):
    """Displays two-dimensional plots clustering.

    """

    def visualize_database(self, database: Database, parameters):
        if database.instance_dimension() != 2:
            raise ModuleExecutionError("Cannot display database with dimension different than two")

        data_tranposed = database.get_transposed()

        f = Figure()
        ax = f.add_subplot(111)
        scatter = ax.scatter(data_tranposed[0], data_tranposed[1])

        return f

    def visualize_clustering(self, database: Database, labels: Labels, parameters) -> Figure:
        if database.instance_dimension() != 2:
            raise ModuleExecutionError("Cannot display database with dimension different than two")

        data_tranposed = database.get_transposed()

        f = Figure()
        ax = f.add_subplot(111)
        scatter = ax.scatter(data_tranposed[0], data_tranposed[1], c=labels.get_labels())

        return f
from typing import Optional

from pyclusters.core.entities import Database, Constraints, Labels
from pyclusters.core.interface import GenericModule, OutputStream, ParametersBag, Confirmation


class DisplayGenericModule(GenericModule):
    """Database, Constraints or labels generics module"""

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add("Database", Confirmation.build(True) )
        parameters.add("Constraints", Confirmation.build(True))
        parameters.add("Labels", Confirmation.build(True))
        return parameters

    def run(self, printer: OutputStream, database: Optional[Database], constraints: Optional[Constraints],
            labels: Optional[Labels], parameters):

        if database:
            printer.println( str(database.get_data()) )

        if constraints:
            printer.println(str(constraints.get_ml()))
            printer.println(str(constraints.get_cl()))

        if labels:
            printer.println(str(labels.get_labels()))

from matplotlib.figure import Figure
from pyclusters.core.entities import Labels, Database, DatabaseFactory
from pyclusters.core.interface import DatabaseTransformerModule, ParametersBag, Range
from sklearn.manifold import TSNE


class TSNEDatabaseTransformerModule(DatabaseTransformerModule):
    """TSNE Database transformer module
    """

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add_range( "learning rate", Range.build( min = 10, max = 1000, default=200 ) )
        parameters.add_range( "Nb iterations", Range.build(min=250, default=1000) )
        return parameters

    def transform(self, database: Database, parameters) -> Database:
        learning_rate = parameters["learning rate"]
        n_iter = int(parameters["Nb iterations"])

        data = database.get_data()
        data_embedded = TSNE(
            n_components=2,
            learning_rate=learning_rate,
            n_iter=n_iter
        ).fit_transform(data)

        return DatabaseFactory.build_from_data(
            data=data_embedded
        )
import unittest
from pyclusters.core.interface import Range, Choices, Confirmation, Entry
from pyclusters.core.interface import ParametersBag, InvalidConfiguationException

class ParametersTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.parameters = ParametersBag.empty()

    def test_range(self):
        # valid range configuration
        range = Range.build(min = 1, max = 100, step = 1)
        self.assertEqual(range.min, 1)
        self.assertEqual(range.max, 100)
        self.assertEqual(range.step, 1)
        self.assertEqual(range.default, 1)

        # invalid range configuration
        with self.assertRaises(InvalidConfiguationException):
            Range.build( min = 1, max=-1 )

        with self.assertRaises(InvalidConfiguationException):
            Range.build( min = "Test" )





def launch_test():
    unittest.main()

if __name__ == '__main__':
    launch_test()

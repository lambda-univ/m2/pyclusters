
from pyclusters.core.interface import DatabaseIOModule, Configurable
from pyclusters.core.entities import Database, Labels, Constraints
from pyclusters.core.entities import DatabaseFactory, LabelsFactory, ConstraintsFactory
from random import randint, choice
import numpy as np

def constrained_clustering( nb_instances, nb_labels, nb_constraints, seed = 111 ) -> (Database, Constraints, Labels):
    # generating database with numpy
    rand = np.random.RandomState(seed)
    database = DatabaseFactory.build_from_data(
        data=rand.normal( 0, 1, nb_instances )
    )

    # generating labels
    # as randint return a int between a and b includes, to return int 0 <= n < nb_labels, we must
    # decrement nb_labels
    labels = [ randint( 0, nb_labels - 1 ) for _ in range(nb_labels) ]

    # generating constraints
    # we want distinct constraints, so we must iterate until there are enough constraints
    # disclaimer: constraints satisfiability constraints is not checked !
    cl = []
    ml = []
    ML_VALUE = 1
    CL_VALUE = -1
    while len(cl) + len(ml) != nb_constraints :
        constraint = randint(0, nb_instances - 1), randint(0, nb_instances)
        type = choice([ ML_VALUE, CL_VALUE ])
        if type == ML_VALUE and constraint not in ml:
            ml.append(constraint)
        if type == CL_VALUE and constraint not in cl:
            cl.append(constraint)

    constraints = ConstraintsFactory.build_from_data(
        ml = ml,
        cl = cl,
    )

    return database, constraints, labels
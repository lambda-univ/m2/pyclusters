from pyclusters.core.interface import Range, Choices, Parameter, ParametersBag, Confirmation, Entry
import gi
gi.require_version('Gtk', '3.0')
import threading

from gi.repository import Gtk, Gdk


class GtkButtonEvent:
    RIGHT_CLICK = 3

def is_right_click( event ):
    return event.type == Gdk.EventType.BUTTON_PRESS and event.button == GtkButtonEvent.RIGHT_CLICK

def focus_treeview_row_hover_cursor(treeview, event):
    pthinfo = treeview.get_path_at_pos(event.x, event.y)
    if pthinfo is not None:
        path, col, cellx, celly = pthinfo
        treeview.grab_focus()
        treeview.set_cursor(path, col, 0)

class ComboBoxUtils:
    """Gtk Combo box utils."""
    @staticmethod
    def has_active(combo):
        """Returns True if given combo has active entry, False otherwise."""
        return combo.get_active_iter() is not None

    @staticmethod
    def get_active(combo):
        """Returns active entry if present, None otherwise."""
        if ComboBoxUtils.has_active(combo):
            tree_iter = combo.get_active_iter()
            model = combo.get_model()
            return model[tree_iter]
        else:
            return None

    @staticmethod
    def get_first_column_of_active(combo):
        """Returns first column of active combo"""
        return ComboBoxUtils.get_active(combo)[0]


class GtkComboBoxManager:
    def __init__(self, gtk_combo):
        self.combo = gtk_combo
    
    def has_active(self) -> bool:
        return ComboBoxUtils.has_active(self.combo)
        
    def first(self):
        """Returns first value in active entry."""
        if self.has_active():
            return ComboBoxUtils.get_active(self.combo)[0]
    
    def at_index(self, index : int):
        if self.has_active():
            return ComboBoxUtils.get_active(self.combo)[index]

def execute_function_on_gtk_ui_thread(callback, *args, **kwargs):
    def cb(_None):
        callback(*args, **kwargs)
        return False

    Gdk.threads_add_idle(GLib.PRIORITY_HIGH, cb, None)


def OnThread(function):
    """Decorates given function in order to execute it in anotehr thread."""
    def wrapper(*args):
        threading.Thread(target=function, args=args).start()

    return wrapper


def OnUIThread(function):
    """Decorated given function in order to execute it in GTK UI Thread."""
    def wrapper(*args, **kwargs):
        execute_function_on_gtk_ui_thread(function, *args, **kwargs)

    return wrapper


class GtkSpinnerManager:
    """Handles GtkSpinner."""
    @staticmethod
    def build(spinner): 
        """Buils and returns a new GTkSpinnerManager in order to manage a spinner."""
        return GtkSpinnerManager(spinner)

    @staticmethod
    def build_with_button(spinner, button):
        """Builds and reutrns a new GtkSpinnerButtonManager which handles a spinner and a button."""
        return GtkSpinnerButtonManager(spinner, button)

    def __init__(self, spinner):
        self.__spinner = spinner

    @OnUIThread
    def __enter__(self):
        self.__spinner.start()

    @OnUIThread
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__spinner.stop()


class GtkSpinnerButtonManager:
    def __init__(self, spinner, button):
        self.__spinner = spinner
        self.__button = button

    @OnUIThread
    def __enter__(self):
        self.__button.set_sensitive(False)
        self.__spinner.start()

    @OnUIThread
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__button.set_sensitive(True)
        self.__spinner.stop()


from gi.repository import Gtk, Gdk, GLib


class GtkProcessThreadUI:
    def __init__(self, window, spinner, button):
        self.window = window
        self.spinner = spinner
        self.button = button

    def __enter__(self):
        self.__start_spinner()
        self.__disable_button()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__stop_spinner()
        self.__enable_button()

    def __start_spinner(self):
        execute_function_on_gtk_ui_thread(self.spinner.start)

    def __stop_spinner(self):
        execute_function_on_gtk_ui_thread(self.spinner.stop)

    def __enable_button(self):
        execute_function_on_gtk_ui_thread(self.button.set_sensitive, True)

    def __disable_button(self):
        execute_function_on_gtk_ui_thread(self.button.set_sensitive, False)

    def display_info(self, title, message=None):
        def show_dialog(window):
            dialog = Gtk.MessageDialog(
                transient_for=window,
                flags=0,
                message_type=Gtk.MessageType.INFO,
                buttons=Gtk.ButtonsType.OK,
                text=title,
            )
            if message:
                dialog.format_secondary_text(
                    message
                )
            dialog.run()
            dialog.destroy()

        execute_function_on_gtk_ui_thread(show_dialog, self.window)

    def display_error(self, message="", title="An error occurred"):
        def show_dialog(window):
            dialog = Gtk.MessageDialog(
                transient_for=window,
                flags=0,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                text=title,
            )
            dialog.format_secondary_text(message)
            dialog.run()
            dialog.destroy()

        execute_function_on_gtk_ui_thread(show_dialog, self.window)

class GtkEntryFactory:
    """GtkEntryFactory is a factory class which builds a Gtk entry object from a parameter."""
    @staticmethod
    def listbox_row_from_range(name, range: Range):
        adjustment = Gtk.Adjustment(
            lower=range.min,
            upper=range.max,
            step_increment=range.step,
            value=range.default
        )
        spinbutton = Gtk.SpinButton()
        spinbutton.set_adjustment(adjustment)

        return GtkEntryFactory.__create_gtk_row_from_name_and_entry(name, spinbutton)

    @staticmethod
    def listbox_row_from_choices(name, choices: Choices):
        choices_combo = Gtk.ComboBoxText()
        choices_combo.set_entry_text_column(0)
        for choice in choices:
            choices_combo.append_text(choice)

        choices_combo.set_active(0)

        return GtkEntryFactory.__create_gtk_row_from_name_and_entry(name, choices_combo)

    @staticmethod
    def listbox_row_from_confirmation(name, confirmation : Confirmation):
        check_editable = Gtk.CheckButton()
        check_editable.set_active(confirmation.value)
        return GtkEntryFactory.__create_gtk_row_from_name_and_entry( name, check_editable )

    @staticmethod
    def listbox_row_from_entry(name, entry : Entry):
        gtk_entry = Gtk.Entry()
        gtk_entry.set_text( entry.value )
        return GtkEntryFactory.__create_gtk_row_from_name_and_entry(name, gtk_entry)

    @staticmethod
    def __create_gtk_row_from_name_and_entry(name, entry):
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        label1 = Gtk.Label(label=name, xalign=0)
        vbox.pack_start(label1, True, True, 0)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        hbox.pack_start(vbox, True, True, 0)
        hbox.pack_start(entry, False, True, 0)

        row = Gtk.ListBoxRow()
        row.add(hbox)
        row.show_all()

        return row, entry


class GtkParametersTracker:
    """
    Major concerns of this class is to conserve a reference to each built entry.
    Then, when parameters are required to build
    """

    def __init__(self):
        self.__ranges = {}
        self.__choices = {}
        self.__confirmation = {}
        self.__entries = {}

    def add_range(self, range_parameter: Parameter):
        name = range_parameter.name
        range = range_parameter.property
        row, entry = GtkEntryFactory.listbox_row_from_range(name, range)
        self.__ranges[name] = entry
        return row

    def add_choices(self, paramter: Parameter):
        name = paramter.name
        choices = paramter.property
        row, entry = GtkEntryFactory.listbox_row_from_choices(name, choices)
        self.__choices[name] = entry
        return row

    def add_confirmation(self, parameter : Parameter):
        name = parameter.name
        confirmation = parameter.property
        row, entry = GtkEntryFactory.listbox_row_from_confirmation(name, confirmation)
        self.__confirmation[name] = entry
        return row

    def add_entry(self, parameter : Parameter):
        name = parameter.name
        entry = parameter.property
        row, entry = GtkEntryFactory.listbox_row_from_entry(name, entry)
        self.__entries[name] = entry
        return row

    def clear(self):
        self.__ranges = {}
        self.__choices = {}
        self.__confirmation = {}
        self.__entries = {}

    def get_parameters(self):
        parameters = {}
        for param_name, param_entry in self.__ranges.items():
            parameters[param_name] = param_entry.get_value()

        for param_name, param_entry in self.__choices.items():
            value = ComboBoxUtils.get_active(param_entry)[0]
            parameters[param_name] = value

        for param_name, param_entry in self.__confirmation.items():
            value = param_entry.get_active()
            parameters[param_name] = value

        for param_name, param_entry in self.__entries.items():
            value = param_entry.get_text()
            parameters[param_name] = value

        return parameters


class GtkListboxParametersManager:
    def __init__(self, listbox):
        self.__listbox = listbox
        self.__params_tracker = GtkParametersTracker()

    def fill(self, parameters: ParametersBag):
        # clear listbox and tracker to display new parameters
        self.clear()

        # stop if given parameters undefined
        if parameters is None:
            self.__listbox.hide()
            return

        # display range parameters
        for range_parameter in parameters.get_ranges():
            gtk_entry = self.__params_tracker.add_range(range_parameter)
            self.__listbox.add(gtk_entry)

        # display choices parameters
        for choices_parameter in parameters.get_choices():
            gtk_entry = self.__params_tracker.add_choices(choices_parameter)
            self.__listbox.add(gtk_entry)

        # display entries
        for entry_parameter in parameters.get_entries():
            gtk_entry = self.__params_tracker.add_entry(entry_parameter)
            self.__listbox.add(gtk_entry)

        # display confirmation
        for confirmation_parameter in parameters.get_confirmation():
            gtk_entry = self.__params_tracker.add_confirmation(confirmation_parameter)
            self.__listbox.add(gtk_entry)
    def get_parameters(self):
        return self.__params_tracker.get_parameters()

    def clear(self):
        self.__params_tracker.clear()
        for child in self.__listbox:
            self.__listbox.remove(child)


class GtkTextViewManager:
    def __init__(self, textview):
        self.textview = textview
        self.textbuffer = self.textview.get_buffer()

    def add_text(self, message):
        self.set_text(self.get_text() + message)

    def clear(self):
        self.set_text("")


    def get_text(self):
        return self.textbuffer.get_text(self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter(), False)

    @OnUIThread
    def set_text(self, message):
        self.textbuffer.set_text(message)

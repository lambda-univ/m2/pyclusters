from pyclusters.core.core import Core
from pyclusters.core.entities import IdentifierManager, ProjectFactory, Project
from pyclusters.core.storage import DatabaseNotFoundException, \
    ProjectAlreadyExistsException
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager




class ProjectCreationController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.wm: WindowsManager = wm
        self.core = core

    def init_view(self, object_accessor):
        self.project_name_entry = object_accessor("project_creation_content_project_name")
        self.project_id_entry = object_accessor("project_creation_content_project_id")
        self.project_description_entry = object_accessor(
            "project_creation_content_project_description"
        )
        self.project_database_combo = object_accessor("project_creation_content_project_database_combo")
        self.project_database_liststore = object_accessor("project_creation_content_database_liststore")

    def start(self):
        # set all fields text as empty
        self.project_id_entry.set_text("")
        self.project_name_entry.set_text("")
        self.project_description_entry.set_text("")

        # put all databases in combo box to let select user which one he want to use
        self.project_database_liststore.clear()
        databases_list = self.core.get_all_databases()
        for database in databases_list:
            database_name, database_id = database.get_name(), database.get_id()
            self.project_database_liststore.append([database_id, database_name])

    def project_name_changed(self, event):
        project_name = self.project_name_entry.get_text()
        try:
            project_id = IdentifierManager.create_identifier_by_name(project_name)
        except ValueError:
            project_id = ""
        self.project_id_entry.set_text(project_id)

    def cancel_project_creation(self, event):
        self.wm.open_content("home_content")

    def create_project(self, event):
        # reject creation if all fields not validated
        if not self.__validate_entries():
            return

        # getting module name
        tree_iter = self.project_database_combo.get_active_iter()
        model = self.project_database_combo.get_model()
        database_id = model[tree_iter][0]
        database = self.core.get_database_by_id(database_id)

        # creating project
        project: Project = ProjectFactory.build(
            id=self.project_id_entry.get_text(),
            name=self.project_name_entry.get_text(),
            description=self.project_description_entry.get_text(),
            database=database
        )

        try:
            self.core.create_project(project)
            self.wm.open_content("home_content")
        except DatabaseNotFoundException as e:
            self.wm.display_error(title="Database Not Found", message=str(e))
        except ProjectAlreadyExistsException as e:
            self.wm.display_error(title="Conflict", message=str(e))

    def __validate_entries(self) -> bool:
        # validate feilds
        validator = Validator()
        validator.add_validation(self.project_name_entry, "Project name", Validator.NOT_EMPTY)
        validator.add_validation(self.project_id_entry, "Project id", [
            Validator.NOT_EMPTY,
            Validator.custom(
                lambda entry: IdentifierManager.is_valid_identifier(entry.get_text()),
                "Invalid project identifier"
            )
        ])
        validator.add_validation(self.project_database_combo, "Module Loader", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations

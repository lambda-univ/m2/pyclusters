from pyclusters.core.core import Core
from pyclusters.core.storage import ProjectNotFoundException
from pyclusters.core.subject_observer import Message
from pyclusters.interface.gtk_utils import GtkButtonEvent, is_right_click, focus_treeview_row_hover_cursor
from pyclusters.interface.windows_manager import GtkController, WindowsManager

import logging as log
from gi.repository import Gtk, Gdk

class HomeController(GtkController):

    def __init__(self, core: Core, windows_manager: WindowsManager):
        self.wm = windows_manager
        self.core = core

    def init_view(self, object_accessor):
        self.project_treeview = object_accessor("home_content_project_treeview")
        self.project_liststore = object_accessor("home_content_project_liststore")
        self.project_liststore_filter = self.project_liststore.filter_new()
        self.project_liststore_filter.set_visible_func(self.__filter_project)
        self.project_filter_entry = object_accessor("home_content_project_filter_entry")

        self.database_treeview = object_accessor("home_content_database_treeview")
        self.database_liststore = object_accessor("home_content_database_liststore")

    def start(self):
        self.__fill_projects_and_databases()

    def project_filter_changed(self, event):
        self.project_liststore_filter.refilter()

    def open_project_creation_dialog(self, *args):
        self.wm.open_content("project_creation_content")

    def open_database_import_dialog(self, *args):
        self.wm.open_content("database_creation_content")

    def select_project(self, *args):
        treeview, treepath, treeviewcolumn = args
        tree_selection = treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        project_id = model[treeiter][0]
        try:
            # signal to core that a project has been selected and jump to project window
            self.core.open_project_with_id(project_id)
            self.wm.open_content("main_content")
        except ProjectNotFoundException as e:
            log.exception(e)
            self.wm.display_error(title="Project Not Found", message=str(e))

    def notify(self, message):
        if message == Message.DATABASE_DELETED or message == Message.PROJECT_DELETED:
            self.__fill_projects_and_databases()

    def __filter_project(self, model, iter, data) -> bool:
        # access to user filter
        # return True if filter content is empty to display all projects
        filter_content = self.project_filter_entry.get_text()
        filter_content = filter_content.lower()
        if not filter_content:
            return True

        # access to projet name
        project_name = model[iter][1]
        project_name = project_name.lower()

        # returns True if project is visible, false otherwise
        # set project visible if project name match entry
        print("{} in {} ? {}".format(filter_content, project_name, filter_content in project_name))
        return filter_content in project_name

    def on_project_click(self, treeview, event):
        # only display when right click is pressed
        if is_right_click( event ): self.__display_project_popup(treeview, event)

    def on_database_click(self, treeview, event):
        if is_right_click(event): self.__display_database_popup(treeview, event)

    def __display_project_popup(self, treeview, event):
        focus_treeview_row_hover_cursor(treeview, event)

        tree_selection = treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        entity_id = model[treeiter][0]
        if not entity_id:
            return

        popup = self.__create_project_popup()
        popup.popup_at_pointer()

    def __display_database_popup(self, treeview, event):

        # fixing a bug where a right click is pressed hover an unselected row.
        # We automatically select row hover cursor where right click happened
        focus_treeview_row_hover_cursor(treeview, event)

        tree_selection = treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        entity_id = model[treeiter][0]
        if not entity_id:
            return

        popup = self.__create_database_popup()
        popup.popup_at_pointer()

    def __create_project_popup(self):
        delete_item = Gtk.MenuItem(label="Delete")
        delete_item.connect("button-press-event", self.__delete_project)

        menu = Gtk.Menu()
        menu.add(delete_item)
        menu.show_all()
        return menu

    def __create_database_popup(self):
        delete_item = Gtk.MenuItem(label="Delete")
        delete_item.connect("button-press-event", self.__delete_database)

        menu = Gtk.Menu()
        menu.add(delete_item)
        menu.show_all()
        return menu

    def __delete_project(self, treeview, event):
        # get active project id
        tree_selection = self.project_treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        project_id = model[treeiter][0]

        confirmation = self.wm.confirm_action(
            title="Project deletion",
            message="Whole project will be deleted except database."
        )
        if confirmation:
            self.core.delete_project(project_id)

    def __delete_database(self, treeview, event):
        # get active project id
        tree_selection = self.database_treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        database_id = model[treeiter][0]

        confirmation = self.wm.confirm_action(
            title="Database deletion",
            message="All projects related with this database will be also deleted."
        )
        if confirmation:
            self.core.delete_database(database_id)

    def __fill_projects_and_databases(self):
        # inserting project in list
        self.project_liststore.clear()
        for project in self.core.get_all_projects():
            self.project_liststore.append([project.get_id(), project.get_name(), project.get_description()])

        # inserting database in list
        self.database_liststore.clear()
        for database in self.core.get_all_databases():
            self.database_liststore.append([database.get_id(), database.get_name(), database.get_description()])
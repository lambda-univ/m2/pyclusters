from pyclusters.core.core import Core
from pyclusters.core.entities import IdentifierManager, Database, DatabaseEntity, DatabaseFactory
from pyclusters.core.interface import DatabaseIOModule
from pyclusters.core.storage import DatabaseAlreadyExistsException
from pyclusters.exceptions.core import InvalidFileFormatError
from pyclusters.exceptions.interface import ModuleExecutionError
from pyclusters.interface.gtk_utils import ComboBoxUtils
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager


class ImportDatabaseController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

    def init_view(self, object_accessor):
        self.database_name_entry = object_accessor("database_import_content_database_name")
        self.database_id_entry = object_accessor("database_import_content_database_id")
        self.database_description_entry = object_accessor(
            "database_import_content_database_description"
        )
        self.database_location_entry = object_accessor("database_import_content_database_location")
        self.modules_loader_combo = object_accessor("database_import_content_module_loader")
        self.modules_liststore = object_accessor("database_creation_content_loader_module_liststore")
        self.dataio_module_by_name = {
            module.__name__: module
            for module in self.core.get_modules_of_class(DatabaseIOModule)
        }

    def search_database_location(self, event):
        database_location = self.wm.select_file()
        if database_location is not None:
            self.database_location_entry.set_text(database_location)

    def start(self):
        # clear all textual fields
        self.database_id_entry.set_text("")
        self.database_name_entry.set_text("")
        self.database_description_entry.set_text("")
        self.database_location_entry.set_text("")

        # search all modules able to load a database
        self.modules_liststore.clear()
        for module in self.core.get_modules_of_class(DatabaseIOModule):
            module_name = module.__name__
            self.modules_liststore.append([module_name])

    def database_name_changed(self, *args):
        project_name = self.database_name_entry.get_text()
        try:
            project_id = IdentifierManager.create_identifier_by_name(project_name)
        except ValueError:
            project_id = ""
        self.database_id_entry.set_text(project_id)

    def cancel_database_import(self, event):
        self.wm.open_content("home_content")

    def select_database_file_location(self, *args):
        print(args)

    def import_database(self, event):
        # import database only where all informations are given
        if not self.__validate_entries():
            return

        # retrieve and inflating module
        module_name = ComboBoxUtils.get_active(self.modules_loader_combo)[0]
        module_class = self.dataio_module_by_name[module_name]
        module: DatabaseIOModule = module_class()

        try:
            # loading data and create database
            filename = self.database_location_entry.get_text()
            data: Database = module.read(filename)
            database: DatabaseEntity = DatabaseFactory.build(
                id=self.database_id_entry.get_text(),
                name=self.database_name_entry.get_text(),
                data=data,
                description=self.database_description_entry.get_text()
            )

            # save database locally
            self.core.import_database(database)


        except DatabaseAlreadyExistsException:
            self.wm.display_error(title="Conflict Error", message="Database id already exists")
        except InvalidFileFormatError as e:
            self.wm.display_error(title="Reading Errir", message="Invalid format file error: {}".format(e.message()))
        except ModuleExecutionError as e:
            self.wm.display_error(title=e.get_title(), message=e.get_message())
        except Exception as e:
            self.wm.display_error("An error occured")
        else:
            # jump to home
            self.wm.open_content("home_content")
            self.wm.display_info(title="Database imported")

    def __validate_entries(self) -> bool:
        # validate feilds
        validator = Validator()
        validator.add_validation(self.database_name_entry, "Database name", Validator.NOT_EMPTY)
        validator.add_validation(self.database_id_entry, "Database id", [
            Validator.NOT_EMPTY,
            Validator.custom(
                lambda entry: IdentifierManager.is_valid_identifier(entry.get_text()),
                "Invalid database identifier"
            )
        ])
        validator.add_validation(self.database_location_entry, "Database location", [
            Validator.NOT_EMPTY,
            Validator.PATH_EXISTS,
            Validator.IS_FILE
        ])
        validator.add_validation(self.modules_loader_combo, "Module Loader", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations

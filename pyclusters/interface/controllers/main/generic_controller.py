import logging as log
from typing import Optional

from pyclusters.core.core import Core
from pyclusters.core.entities import LabelsEntity, ConstraintsEntity, Constraints, Labels, Database
from pyclusters.core.interface import GenericModule, OutputStream
from pyclusters.exceptions.core import DatabaseNotFoundException, ConstraintsNotFoundException
from pyclusters.exceptions.interface import InvalidConfiguationException, ModuleExecutionError
from pyclusters.interface.gtk_utils import ComboBoxUtils, GtkListboxParametersManager, \
    GtkSpinnerManager, OnThread, OnUIThread, GtkTextViewManager
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager

class GtkTextViewOutputStream(OutputStream):
    def __init__(self, textview):
        self.textview = textview
        self.textbuffer = self.textview.get_buffer()

    def print(self, message):
        self.__append_text( message )

    def println(self, message):
        self.__append_text(message + "\n")

    @OnUIThread
    def __append_text(self, message ):
        self.textbuffer.set_text( self.textbuffer.get_text( self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter(), False ) + message )
    @OnUIThread
    def clear(self):
        self.textbuffer.set_text("")


class MainGenericController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

        self.generics_modules = {
            module.__name__: module
            for module in self.core.get_modules_of_class(GenericModule)
        }


    def init_view(self, object_accessor):
        self.database_combo = object_accessor("main_content_generics_database_combo")

        self.constraints_combo = object_accessor("main_content_generics_constraints_combo")
        self.contraints_liststore = object_accessor("main_content_generics_constraints_liststore")

        self.labels_combo = object_accessor("main_content_generics_labels_combo")
        self.labels_liststore = object_accessor("main_content_generics_labels_liststore")

        self.generics_modules_combo = object_accessor("main_content_generics_module_combo")
        self.generics_modules_liststore = object_accessor("main_content_generics_module_liststore")
        self.generics_modules_details = object_accessor("main_content_generic_module_details_textview")
        self.generics_modules_details_manager = GtkTextViewManager(self.generics_modules_details)

        self.parameters_container = object_accessor("main_content_generics_module_parameters_container")
        self.parameters_listbox = object_accessor("main_content_generics_module_parameters_listbox")
        self.parameters_manager = GtkListboxParametersManager( self.parameters_listbox )

        self.execute_button = object_accessor("main_content_generics_execute_button")
        output_text = object_accessor("main_content_generics_output_text")
        self.output_stream = GtkTextViewOutputStream(output_text)


        self.spinner = object_accessor("main_content_generics_spinner")


    def start(self):
        # initially hide module parameters
        self.parameters_container.hide()

        self.__fill_generics_module()
        self.__fill_constraints()
        self.__fill_labels()

    def project_updated(self):
        self.__fill_constraints()
        self.__fill_labels()

    @OnThread
    def launch_generic(self, *args):
        # ensures that all fields are valid
        if not self.__validate_fields():
            return

        # clear ouput text
        self.output_stream.clear()
        # retrieve database by id if specified
        database : Optional[Database] = None
        if ComboBoxUtils.has_active(self.database_combo):
            try:
                database_id = self.__get_chosen_database_id()
                database = self.core.get_open_project().get_database_or_view_by_id(database_id).get_database()
            except DatabaseNotFoundException as e:
                log.warning("Selected database was not found !")

        # retrieve constraint by id if specified
        constraints : Optional[Constraints] = None
        if ComboBoxUtils.has_active(self.constraints_combo):
            try:
                constraints_id = self.__get_chosen_constraints_id()
                constraints = self.core.get_open_project().get_constraints_by_id(constraints_id).get_constraints()
            except ConstraintsNotFoundException as e:
                log.warning("Selected constraints was not found !")

        # retrieve labels by id if specified
        labels : Optional[Labels] = None
        if ComboBoxUtils.has_active(self.labels_combo):
            try:
                labels_id = self.__get_chosen_labels_id()
                labels = self.core.get_open_project().get_labels_by_id(labels_id).get_labels()
            except DatabaseNotFoundException as e:
                log.warning("Selected labels was not found !")

        # retrieve module by his name
        module_name = self.__get_chosen_generic_module_name()
        module_class = self.generics_modules[module_name]
        module : GenericModule = module_class()
        parameters = self.parameters_manager.get_parameters()

        # execute module
        # no need to notice user that everything run, an ouput will do it for us$
        with GtkSpinnerManager.build_with_button(self.spinner, self.execute_button):
            try:
                module.run(
                    printer=self.output_stream,
                    database=database,
                    constraints=constraints,
                    labels=labels,
                    parameters=parameters
                )
            except InvalidConfiguationException as e:
                self.wm.display_error(title="Module Error", message="Module configuration failed: {}".format(str(e)))
            except ModuleExecutionError as e:
                log.exception(e)
                self.wm.display_error(title=e.get_title(), message=e.get_message())
            except Exception as e:
                log.exception(e)
                self.wm.display_error(title="An Error Occured", message=str(e))



    def on_generic_database_changed(self, *ignored):
        self.__fill_constraints()
        self.__fill_labels()

    def on_generic_module_changed(self, *ignored):
        # clear parameters
        self.parameters_manager.clear()

        # if no module specified hide parameters
        if not ComboBoxUtils.has_active(self.generics_modules_combo):
            self.parameters_container.hide()
            return

        # retrieve and fill parameters
        generic_module_name = self.__get_chosen_generic_module_name()
        module_class = self.generics_modules[ generic_module_name ]
        module : GenericModule = module_class()
        parameters = module.require()
        self.parameters_manager.fill(parameters)

        # if no parameters, hide parameters
        if parameters.has_parameters():
            self.parameters_container.show()
        else:
            self.parameters_container.hide()

        # fill documentation
        module_documentation = module_class.__doc__
        if module_documentation:
            self.generics_modules_details_manager.set_text(module_documentation)
        else:
            self.generics_modules_details_manager.set_text("")

    def __fill_constraints(self):
        # clear contraints
        self.contraints_liststore.clear()

        # do nothing if no database
        if not ComboBoxUtils.has_active(self.database_combo):
            return

        # get database id and search constraints related with specified database
        project = self.core.get_open_project()
        database_id = self.__get_chosen_database_id()
        contraints : [ConstraintsEntity] = project.get_constraints_related_with_database(database_id)

        # fill constraints liststore
        for constraint in contraints:
            self.contraints_liststore.append([  constraint.get_id(), constraint.get_name() ])


    def __fill_labels(self):
        # clear labels
        self.labels_liststore.clear()

        # do nothing if no database
        if not ComboBoxUtils.has_active(self.database_combo):
            return

        # get database id and search labels related with specified database
        project = self.core.get_open_project()
        database_id = self.__get_chosen_database_id()
        labels: [LabelsEntity] = project.get_labels_related_with_database(database_id)

        # fill labels into liststore
        for label in labels:
            self.labels_liststore.append([label.get_id(), label.get_name()])


    def __validate_fields(self):
        # validate feilds
        validator = Validator()
        validator.add_validation(self.generics_modules_combo, "Generic Module", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations

    def __fill_generics_module(self):
        self.generics_modules_liststore.clear()
        for module_name in self.generics_modules:
            self.generics_modules_liststore.append([module_name])

    def __get_chosen_database_id(self):
        if not ComboBoxUtils.has_active( self.database_combo ):
            raise AssertionError("No database selected")
        return ComboBoxUtils.get_active(self.database_combo)[0]

    def __get_chosen_generic_module_name(self):
        if not ComboBoxUtils.has_active(self.generics_modules_combo):
            raise AssertionError("No generic module selected")
        return ComboBoxUtils.get_active(self.generics_modules_combo)[0]

    def __get_chosen_constraints_id(self):
        if not ComboBoxUtils.has_active(self.constraints_combo):
            raise AssertionError("No constraints selected")
        return ComboBoxUtils.get_active(self.constraints_combo)[0]

    def __get_chosen_labels_id(self):
        if not ComboBoxUtils.has_active(self.labels_combo):
            raise AssertionError("No labels selected")
        return ComboBoxUtils.get_active(self.labels_combo)[0]


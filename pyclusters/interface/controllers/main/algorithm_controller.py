from pyclusters.core.core import Core
from pyclusters.core.entities import IdentifierManager, LabelsEntity, LabelsFactory, Database, DatabaseEntity, Labels
from pyclusters.core.interface import ClusteringAlgorithmModule
from pyclusters.exceptions.core import InvalidLabelsError
from pyclusters.exceptions.interface import InvalidConfiguationException, ModuleExecutionError, InvalidModuleReturnError
from pyclusters.interface.gtk_utils import ComboBoxUtils, GtkListboxParametersManager, \
    GtkSpinnerManager, OnThread, GtkTextViewManager
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager


import logging  as log

class MainAlgorithmController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

        self.clustering_modules = {
            module.__name__: module
            for module in self.core.get_modules_of_class(ClusteringAlgorithmModule)
        }

    def init_view(self, object_accessor):
        self.name_entry = object_accessor("main_content_algorithms_name_entry")
        self.id_entry = object_accessor("main_content_algorithms_id_entry")

        self.database_combo = object_accessor("main_content_algorithms_database_combo")
        self.constraints_combo = object_accessor("main_content_algorithms_constraints_combo")
        self.constraints_liststore = object_accessor("main_content_algorithms_constraints_liststore")

        self.algorithm_combo = object_accessor("main_content_algorithms_algorithm_combo")
        self.algorithm_details_textview = object_accessor("main_content_algorithm_module_details_textview")
        self.algorithm_details_manager = GtkTextViewManager( self.algorithm_details_textview )
        self.algorithm_parameters_listbox = object_accessor("main_content_algorithms_parameters_listbox")
        self.parameters_manager = GtkListboxParametersManager(self.algorithm_parameters_listbox)
        self.algorithm_liststore = object_accessor("main_content_algorithms_module_liststore")
        self.algorithm_parameters_container = object_accessor("main_content_algorithm_module_parameters_container")

        self.algorithm_runner_spinner = object_accessor("main_content_algorithm_spinner")
        self.execute_button = object_accessor("main_content_algorithm_execute_button")

    def start(self):
        # At starting, no algorithm module is choose, so hide parameters container.
        self.algorithm_parameters_container.hide()

        self.__fill_algorithm_modules()


    def algorithms_database_changed(self, *args):
        self.__fill_constraints()

    def __fill_constraints(self):
        """
        Fills database related constraints in list store.
        """
        # first thing to do is to clean liststore
        self.constraints_liststore.clear()

        # found database id or stop if not specified
        if not ComboBoxUtils.has_active(self.database_combo):
            return
        database_id = ComboBoxUtils.get_active(self.database_combo)[0]

        # fill constraints related with a database
        project = self.core.get_open_project()
        for constraints in project.get_constraints_related_with_database(database_id):
            self.constraints_liststore.append([constraints.get_id(), constraints.get_name()])

    def algorithm_result_name_changed(self, event):
        try:
            name = self.name_entry.get_text()
            self.id_entry.set_text(IdentifierManager.create_identifier_by_name(name) if name else "")
        except ValueError:
            self.id_entry.set_text("")

    @OnThread
    def on_execute_button_clicked(self, event):
        # validates fields
        if not self.__validate_fields():
            return

        with GtkSpinnerManager.build_with_button(self.algorithm_runner_spinner, self.execute_button):
            try:
                # run algorithm
                self.__execute_algorithms()

                # notify user that everything is okay :)
                self.wm.display_info(title="Labels set generated successfully")
            except InvalidLabelsError as e:
                self.wm.display_error(
                    title="Module Error",
                    message="Returned labels set is incompatible with selected database: {}".format(e)
                )
            except InvalidConfiguationException as e:
                self.wm.display_error(title="Module Error", message="Module configuration failed: {}".format(str(e)))
            except ModuleExecutionError as e:
                log.exception(e)
                self.wm.display_error(title=e.get_title(), message=e.get_message())
            except Exception as e:
                log.exception(e)
                self.wm.display_error(title="An Error Occurred", message=str(e))

    def __execute_algorithms(self):
        # get opened project
        project = self.core.get_open_project()

        # retrieve module by his name
        module_name = ComboBoxUtils.get_active(self.algorithm_combo)[0]
        module_class = self.clustering_modules[module_name]
        module: ClusteringAlgorithmModule = module_class()

        # retrieve constraints
        constraints_id = ComboBoxUtils.get_active(self.constraints_combo)[0]
        constraints = project.get_constraints_by_id(constraints_id)

        # retrieve parameters
        parameters = self.parameters_manager.get_parameters()

        # retrieve database
        database_id = ComboBoxUtils.get_active(self.database_combo)[0]
        database : DatabaseEntity = project.get_database_or_view_by_id(database_id)
        assert database is not None, "Database not found !"

        # run module to get labels
        labels_data : Labels = module.predict(
            database=database.get_database(),
            constraints=constraints.get_constraints(),
            parameters=parameters
        )

        # ensures that returned data is labels
        if not isinstance(labels_data, Labels):
            raise InvalidModuleReturnError(
                message="Returned value isn't Labels entity: got {}".format( labels_data )
            )

        # creating labels entity
        labels_entity: LabelsEntity = LabelsFactory.build(
            id=self.id_entry.get_text(),
            name=self.name_entry.get_text(),
            database_id=database_id,
            labels=labels_data
        )

        # transmit labels set to core in order to add it to proejct
        self.core.add_labels_to_project(project, labels_entity)

    def algorithm_module_changed(self, *args):
        self.parameters_manager.clear()

        if not ComboBoxUtils.has_active(self.algorithm_combo):
            self.algorithm_parameters_container.hide()
            return

        # retrieve module class by his name
        module_name = ComboBoxUtils.get_active(self.algorithm_combo)[0]
        module_class = self.clustering_modules[module_name]

        # put parameters in listbox
        module: ClusteringAlgorithmModule = module_class()
        parameters = module.require()
        self.parameters_manager.fill(parameters)
        if parameters.has_parameters():
            self.algorithm_parameters_container.show()
        else:
            self.algorithm_parameters_container.hide()


        # update module documentation
        module_doc = module_class.__doc__
        if module_doc:
            self.algorithm_details_manager.set_text(module_doc)
        else:
            self.algorithm_details_manager.set_text("")

    def __fill_algorithm_modules(self):
        # clear modules and insert them a new once
        self.algorithm_liststore.clear()
        modules = list(self.clustering_modules.values())
        for module in modules:
            self.algorithm_liststore.append([module.__name__])

        # if module present, set first active
        if modules:
            self.algorithm_combo.set_active(0)

    def __validate_fields(self):
        # validate feilds
        validator = Validator()
        validator.add_validation(self.name_entry, "Result name", Validator.NOT_EMPTY)
        validator.add_validation(self.id_entry, "Result id", [
            Validator.NOT_EMPTY,
            Validator.custom(
                lambda entry: IdentifierManager.is_valid_identifier(entry.get_text()),
                "Invalid database identifier"
            )
        ])
        validator.add_validation(self.database_combo, "Database", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.constraints_combo, "Constraints", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.algorithm_combo, "Algorithm", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations

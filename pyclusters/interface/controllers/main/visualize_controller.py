from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
from pyclusters.core.core import Core
from pyclusters.core.entities import LabelsEntity, DatabaseEntity
from pyclusters.core.interface import DatabaseVisualizerModule, ClusteringVisualizerModule, Configurable, ParametersBag
from pyclusters.exceptions.interface import ModuleExecutionError
from pyclusters.interface.gtk_utils import ComboBoxUtils, GtkListboxParametersManager, \
    GtkSpinnerManager, OnThread, OnUIThread, GtkTextViewManager
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager


class MainVisualizerController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

        self.databases_modules = {
            module.__name__: module
            for module in self.core.get_modules_of_class(DatabaseVisualizerModule)
        }

        self.clusterings_modules = {
            module.__name__: module
            for module in self.core.get_modules_of_class(ClusteringVisualizerModule)
        }

    def init_view(self, object_accessor):
        self.labels_set_combo = object_accessor("main_content_visualize_labels_set_combo")
        self.labels_liststore = object_accessor("main_content_visualize_labels_store")

        self.flowbow = object_accessor("main_content_visualizer_flowbox")
        self.database_combo = object_accessor("main_content_visualize_database_combo")

        self.module_combo = object_accessor("main_content_visualize_module_combo")
        self.modules_liststore = object_accessor("main_content_visualizer_module_liststore")
        self.module_details_textview = object_accessor("main_content_visualize_module_details_textview")
        self.module_details_manager = GtkTextViewManager(self.module_details_textview)
        self.module_parameters_listbox = object_accessor("main_content_visualize_parameters_listbox")
        self.module_parameters_container = object_accessor("main_content_visualizer_parameters_container")
        self.parameters_manager = GtkListboxParametersManager(self.module_parameters_listbox)

        self.visualize_spinner = object_accessor("main_content_visualize_spinner")
        self.visualize_button = object_accessor("main_content_visualize_button")

    def start(self):
        self.__project = self.core.get_open_project()

        # init visualizer modules
        self.__fill_visualizer_modules()
        self.__fill_labels()

    def project_updated(self):
        self.__fill_labels()

    def visualize_labels_set_changed(self, *args):
        self.__fill_visualizer_modules()

    def visualize_clear_labels(self, event):
        self.labels_set_combo.set_active(-1)
        self.__fill_visualizer_modules()

    def __fill_visualizer_modules(self):
        # when modules changed, clear parameters
        self.module_parameters_container.hide()
        self.parameters_manager.clear()

        # if no labels set specified, used database visualizer modules
        if ComboBoxUtils.has_active(self.labels_set_combo):
            modules = self.clusterings_modules.values()
        else:
            modules = self.databases_modules.values()

        self.modules_liststore.clear()
        for module in modules:
            self.modules_liststore.append([module.__name__])

    def visualizer_module_changed(self, event):
        # as module isn't always activated, clear module details if no one is active
        if not self.__is_visualizer_module_active():
            self.module_details_manager.set_text("")
            return

        module_class = self.__get_active_visualizer_module_class()

        # instance module to access parameters
        module = module_class()
        self.__display_visualizer_module_parameters(module)

        # displays module documentation
        module_documentation = module_class.__doc__
        if module_documentation:
            self.module_details_manager.set_text(module_documentation)
        else:
            self.module_details_manager.set_text("")


    def delete_figure(self, *args):
        visualizer_flowbow, visualizer_child = args
        visualizer_flowbow.remove(visualizer_child)

    def visualize_database_changed(self, *args):
        self.__fill_labels()

    @OnThread
    def visualize_data(self, event):
        try:
            with GtkSpinnerManager.build_with_button(self.visualize_spinner, self.visualize_button):
                if ComboBoxUtils.has_active(self.labels_set_combo):
                    self.__visualize_database_and_labels()
                else:
                    self.__visualize_database()
        except ModuleExecutionError as e:
            self.wm.display_error( title=e.get_title(), message=e.get_message() )
        except Exception as e:
            self.wm.display_error(title="An error occured", message=str(e))

    def __visualize_database_and_labels(self):
        validator = Validator()
        validator.add_validation(self.database_combo, "Database", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.labels_set_combo, "Labels Set", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.module_combo, "Visualizer Module", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)
            return

        # retrieve module and parameters
        module_name = ComboBoxUtils.get_active(self.module_combo)[0]
        module_class = self.databases_modules[module_name]
        module: ClusteringVisualizerModule = module_class()
        parameters = self.parameters_manager.get_parameters()

        # retrieve database
        database_id = ComboBoxUtils.get_active(self.database_combo)[0]
        database : DatabaseEntity = self.core.get_database_by_id(database_id)

        # retrieve labels set
        project = self.core.get_open_project()
        labels_set_id = ComboBoxUtils.get_active(self.labels_set_combo)[0]
        labels_set : LabelsEntity = project.get_labels_by_id(labels_set_id)


        f = module.visualize_clustering(database.get_database(), labels_set.get_labels(), parameters)
        self.__add_figure_on_flowbow(f)

    def __visualize_database(self):
        validator = Validator()
        validator.add_validation(self.database_combo, "Database", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.module_combo, "Visualizer Module", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)
            return

        module_name = ComboBoxUtils.get_active(self.module_combo)[0]
        module_class = self.databases_modules[module_name]
        module: DatabaseVisualizerModule = module_class()
        parameters = self.parameters_manager.get_parameters()

        # retrieve database
        database_id = ComboBoxUtils.get_active(self.database_combo)[0]
        database : DatabaseEntity = self.__project.get_database_or_view_by_id(database_id)

        f = module.visualize_database(database.get_database(), parameters)
        self.__add_figure_on_flowbow(f)

    @OnUIThread
    def __add_figure_on_flowbow(self, figure):
        canvas = FigureCanvas(figure)  # a Gtk.DrawingArea
        canvas.show()
        self.flowbow.add(canvas)

    def __is_visualizer_module_active(self):
        return ComboBoxUtils.has_active(self.module_combo)

    def __get_active_visualizer_module_class(self):
        module_name = ComboBoxUtils.get_active(self.module_combo)[0]
        if ComboBoxUtils.has_active(self.labels_set_combo):
            print(self.clusterings_modules, module_name)
            module_class = self.clusterings_modules[module_name]
            return module_class
        else:
            module_class = self.databases_modules[module_name]
            return module_class

    def __display_visualizer_module_parameters(self, module: Configurable):
        # This thing to do is to clean parameters
        self.parameters_manager.clear()

        if module is None:
            return

        # start by cleaning visualizer module parameters
        for child in self.module_parameters_listbox:
            self.module_parameters_listbox.remove(child)

        # if no parameters, hide parameters section and stop
        parameters: ParametersBag = module.require()
        if not parameters.has_parameters():
            self.module_parameters_container.hide()
            return

        # visualiser has parameters, so show parameters container
        self.module_parameters_container.show()
        self.parameters_manager.fill(parameters)

    def __fill_labels(self):
        """
        Fills database related constraints in list store.
        """
        # first thing to do is to clean liststore
        self.labels_liststore.clear()

        # found database id or stop if not specified
        if not ComboBoxUtils.has_active(self.database_combo):
            return


        # fill constraints related with a database
        project = self.core.get_open_project()
        database_id = ComboBoxUtils.get_active(self.database_combo)[0]
        for labels in project.get_labels_related_with_database(database_id):
            self.labels_liststore.append([labels.get_id(), labels.get_name()])

import logging as log

from pyclusters.core.core import Core
from pyclusters.core.entities import Project
from pyclusters.core.interface import ComparatorModule, Configurable
from pyclusters.exceptions.interface import InvalidConfiguationException, ModuleExecutionError
from pyclusters.interface.controllers.main.generic_controller import GtkTextViewOutputStream
from pyclusters.interface.gtk_utils import ComboBoxUtils, GtkListboxParametersManager, \
    GtkSpinnerManager, OnThread, GtkTextViewManager
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager


class MainComparatorController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

        self.comparators_modules = {
            module.__name__: module
            for module in self.core.get_modules_of_class(ComparatorModule)
        }


    def init_view(self, object_accessor):
        self.first_labels_combo = object_accessor("main_content_comparators_first_labels_combo")
        self.second_labels_combo = object_accessor("main_content_comparators_second_labels_combo")
        self.labels_liststore = object_accessor("main_content_comparators_labels_liststore")


        self.modules_combo = object_accessor("main_content_comparators_module_combo")
        self.modules_liststore = object_accessor("main_content_comparators_module_liststore")
        module_details_textview = object_accessor("main_content_comparators_module_details_textview")
        self.module_details_manager = GtkTextViewManager( module_details_textview )

        self.parameters_container = object_accessor("main_content_comparators_parameters_container")
        self.parameters_listbox = object_accessor("main_content_comparators_parameters_listbox")
        self.parameters_manager = GtkListboxParametersManager(self.parameters_listbox)

        output_text = object_accessor("main_content_comparators_output_textview")
        self.output_stream = GtkTextViewOutputStream(output_text)

        self.execute_button = object_accessor("main_content_comparators_compare_button")
        self.spinner = object_accessor("main_content_generics_spinner")


    def start(self):
        # initially hide module parameters
        self.parameters_container.hide()

        self.__fill_comparators_module()
        self.__fill_labels()

    def project_updated(self):
        self.__fill_labels()


    def on_comparator_module_changed(self, *args):
        # if no comparator choose, hide parameters
        if ComboBoxUtils.has_active(self.modules_combo):
            self.__fill_comparator_details()
            self.__fill_comparator_parameters()
        else:
            self.__clear_comparator_details()
            self.__clear_comparator_parameters()

    @OnThread
    def on_compare_button_clicked(self, *args):
        if not self.__validate_fields():
            return

        try:
            with GtkSpinnerManager.build_with_button(self.spinner, self.execute_button):
                self.__compare_labels()
        except InvalidConfiguationException as e:
            self.wm.display_error(title="Module Error", message="Module configuration failed: {}".format(str(e)))
        except ModuleExecutionError as e:
            log.exception(e)
            self.wm.display_error(title=e.get_title(), message=e.get_message())
        except Exception as e:
            log.exception(e)
            self.wm.display_error(title="An Error Occured", message=str(e))

    def __compare_labels(self):
        self.output_stream.clear()

        project = self.core.get_open_project()

        first_labels_id = ComboBoxUtils.get_active(self.first_labels_combo)[0]
        first_labels = project.get_labels_by_id(first_labels_id)

        second_labels_id = ComboBoxUtils.get_active(self.second_labels_combo)[0]
        second_labels = project.get_labels_by_id(second_labels_id)

        parameters = self.parameters_manager.get_parameters()

        module_name = ComboBoxUtils.get_active(self.modules_combo)[0]
        module_cls = self.comparators_modules[module_name]
        module: ComparatorModule = module_cls()

        module.compare( self.output_stream, first_labels.get_labels(), second_labels.get_labels(), parameters )

    def __validate_fields(self):
        validator = Validator()
        validator.add_validation(self.first_labels_combo, "First Labels", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.second_labels_combo, "Second Labels", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations


    def __fill_comparator_parameters(self):
        module_name = ComboBoxUtils.get_active(self.modules_combo)[0]
        module_cls = self.comparators_modules[module_name]
        module : Configurable = module_cls()
        parameters = module.require()
        self.parameters_manager.fill(parameters)

        if parameters.has_parameters():
            self.parameters_container.show()
        else:
            self.parameters_container.hide()

    def __clear_comparator_parameters(self):
        self.parameters_manager.clear()
        self.parameters_container.hide()


    def __fill_comparator_details(self):
        module_name = ComboBoxUtils.get_active(self.modules_combo)[0]
        module_cls = self.comparators_modules[module_name]
        module_documentation = module_cls.__doc__
        if module_documentation:
            self.module_details_manager.set_text(module_documentation)
        else:
            self.__clear_comparator_details()

    def __clear_comparator_details(self):
        self.module_details_manager.clear()

    def __fill_comparators_module(self):
        # clears comparators module
        self.modules_liststore.clear()

        for comparator in self.comparators_modules.values():
            self.modules_liststore.append([ comparator.__name__ ])

    def __fill_labels(self):
        # clear existing labels from liststore
        self.labels_liststore.clear()

        project : Project = self.core.get_open_project()
        for labels in project.get_labels():
            self.labels_liststore.append([ labels.get_id(), labels.get_name() ])






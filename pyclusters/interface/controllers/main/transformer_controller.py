from pyclusters.core.core import Core
from pyclusters.core.entities import IdentifierManager, IdentifierCreationException, DatabaseFactory, Database, \
    DatabaseEntity
from pyclusters.core.interface import DatabaseTransformerModule
from pyclusters.exceptions.core import ConflictException
from pyclusters.exceptions.interface import InvalidConfiguationException, ModuleExecutionError
from pyclusters.interface.gtk_utils import GtkListboxParametersManager, ComboBoxUtils, GtkTextViewManager, OnThread, \
    GtkSpinnerManager
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager


class MainTransformerController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

        self.transformers_modules_by_name = {
            module.__name__: module
            for module in self.core.get_modules_of_class(DatabaseTransformerModule)
        }


    def init_view(self, object_accessor):
        self.database_combo = object_accessor("main_content_transform_database_combo")

        self.name_entry = object_accessor("main_content_transform_view_name_entry")
        self.id_entry = object_accessor("main_content_transform_view_id_entry")

        self.modules_combo = object_accessor("main_content_transform_module_combo")
        self.modules_liststore = object_accessor("main_content_transformers_liststore")
        self.modules_details_textview = object_accessor("main_content_transform_module_details_textview")
        self.modules_details_managar = GtkTextViewManager(self.modules_details_textview)

        self.parameters_container = object_accessor("main_content_transform_module_parameters_container")
        self.parameters_listbox = object_accessor("main_content_transformers_parameters_listbox")
        self.parameters_manager = GtkListboxParametersManager(self.parameters_listbox)

        self.execute_button = object_accessor("main_content_transform_button")
        self.spinner = object_accessor("main_content_transform_spinner")


    def start(self):
        # initially hide module parameters
        self.parameters_container.hide()

        # fill view
        self.__fill_transformers_module()

    @OnThread
    def on_transform_button_clicked(self, *args):
        with GtkSpinnerManager.build_with_button(self.spinner, self.execute_button):
            self.__transform_database()


    def __transform_database(self):
        if not self.__validate_fields():
            return

        module_class = self.__get_chosen_module_class()
        module : DatabaseTransformerModule = module_class()
        parameters = self.parameters_manager.get_parameters()
        database : DatabaseEntity = self.__get_database()

        try:
            view_data : Database = module.transform( database.get_database(), parameters )
            view : DatabaseEntity = DatabaseFactory.build(
                id = self.id_entry.get_text(),
                name = self.name_entry.get_text(),
                data = view_data.get_data(),
                description="",
                headers=view_data.get_headers()
            )
            self.core.add_view( self.core.get_open_project(), view )
        except ConflictException:
            self.wm.display_error(title="Conflict", message="A view with the same id already exists !")
        except InvalidConfiguationException as e:
            self.wm.display_error(title="Module Error", message="Module configuration failed: {}".format(str(e)))
        except ModuleExecutionError as e:
            self.wm.display_error(title=e.get_title(), message=e.get_message())
        except Exception as e:
            self.wm.display_error(title="An Error Occured", message=str(e))




    def on_view_name_changed(self, *args):
        view_name = self.name_entry.get_text()
        try:
            view_id = IdentifierManager.create_identifier_by_name(view_name)
            self.id_entry.set_text(view_id)
        except IdentifierCreationException:
            self.id_entry.set_text("")


    def on_transform_module_changed(self, *args):
        if ComboBoxUtils.has_active(self.modules_combo):
            self.__fill_module_parameters()
            self.__fill_module_documentation()
        else:
            self.__clear_module_parameters()
            self.__clear_module_documentation()

    def __clear_module_documentation(self):
        self.modules_details_managar.clear()

    def __clear_module_parameters(self):
        self.parameters_container.hide()
        self.parameters_manager.clear()

    def __fill_module_documentation(self):
        # retrieve a module by his name as it's the only information we can store in combo
        module_class = self.__get_chosen_module_class()
        module_doc = module_class.__doc__ if module_class.__doc__ else ""
        self.modules_details_managar.set_text(module_doc)

    def __fill_module_parameters(self):
        # retrieve a module by his name as it's the only information we can store in combo
        module_class = self.__get_chosen_module_class()
        module : DatabaseTransformerModule = module_class()
        parameters = module.require()

        # if module doesn't require any parameter, clear parameters list and hide parameters container
        if not parameters.has_parameters():
            self.__clear_module_parameters()
            return

        # to display parameters, container must be visible
        self.parameters_container.show()
        self.parameters_manager.fill(parameters)


    def __fill_transformers_module(self):
        # clear liststore
        self.modules_liststore.clear()
        for module in self.__get_transformers_modules():
            self.modules_liststore.append([module.__name__])


    def __get_transformers_modules(self): return list(self.transformers_modules_by_name.values())

    def __get_chosen_module_class(self):
        if ComboBoxUtils.has_active(self.modules_combo):
            module_name = ComboBoxUtils.get_active(self.modules_combo)[0]
            module = self.transformers_modules_by_name[module_name]
            return module
        else:
            ValueError("No transformers chosen")

    def __get_database(self) -> DatabaseEntity:
        database_id = ComboBoxUtils.get_active(self.database_combo)[0]
        database : DatabaseEntity = self.core.get_open_project().get_database_or_view_by_id(database_id)
        return database


    def __validate_fields(self):
        # validate feilds
        validator = Validator()
        validator.add_validation(self.database_combo, "Database", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.modules_combo, "Module", Validator.COMBO_HAS_CHOICE)
        validator.add_validation(self.name_entry, "View name", Validator.NOT_EMPTY)
        validator.add_validation(self.id_entry, "View id", [
            Validator.NOT_EMPTY,
            Validator.custom(
                validator=lambda entry: IdentifierManager.is_valid_identifier(entry.get_text()),
                message="Invalid view id"
            )
        ])
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations
from typing import Optional

from pyclusters.core.core import Core
from pyclusters.core.entities import DatabaseEntity
from pyclusters.core.subject_observer import Message
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager
from gi.repository import Gtk, Gdk

class MainController(GtkController):
    DATABASE_NAME = "Database"
    VIEWS_NAME = "Views"
    CONSTRAINTS_NAME = "Constraints"
    LABELS_NAME = "Labels"
    RIGHT_CLICK = 3

    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

    def init_view(self, object_accessor):
        self.database_liststore = object_accessor("main_content_database_liststore")
        self.project_treestore = object_accessor("main_content_project_treestore")
        self.project_treeview = object_accessor("main_content_project_details_treeview")


        # for renaming
        self.rename_entry = object_accessor("main_content_rename_entry")
        self.rename_container = object_accessor("main_content_rename_container")
        self.rename_type : Optional[str] = None
        self.rename_id : Optional[str] = None




    def project_updated(self):
        self.__fill_project_liststore()
        self.__fill_database_liststore()

    def on_project_details_click(self, treeview, event):
        self.__display_popup_menu_hover_project_details_row(treeview, event)

    def __display_popup_menu_hover_project_details_row(self, treeview, event):
        # only display when right click is pressed
        if event.type != Gdk.EventType.BUTTON_PRESS or event.button != MainController.RIGHT_CLICK:
            return

        # fixing a bug where a right click is pressed hover an unselected row.
        # We automatically select row hover cursor whan right clicked happened
        pthinfo = treeview.get_path_at_pos(event.x, event.y)
        if pthinfo is not None:
            path, col, cellx, celly = pthinfo
            treeview.grab_focus()
            treeview.set_cursor(path, col, 0)

        # As we are in treeview, components are located under a parent item.
        # But parent items are also selectable.
        # So we won't display a popup when right click is pressed hover parent
        tree_selection = self.project_treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        entity_id = model[treeiter][0]
        if not entity_id:
            return

        # finally, we won't display popup hover database row
        parent_iter = model.iter_parent(treeiter)
        entity_type = model[parent_iter][1]
        if entity_type == MainController.DATABASE_NAME:
            return


        popup = self.__create_project_details_popup()
        popup.popup_at_pointer()

    def __on_popup_rename_start(self, *args):
        tree_selection = self.project_treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        entity_id, entity_name, *_ = model[treeiter]
        parent_iter = model.iter_parent(treeiter)
        parent = model[parent_iter][1]

        # update rename entity in order to retreive name
        self.rename_id = entity_id
        self.rename_type = parent
        self.rename_entry.set_text(entity_name)
        self.rename_container.show()
        self.rename_entry.grab_focus()

    def on_rename(self, event):
        assert self.rename_id is not None, "Rename id is None !"
        assert self.rename_type is not None, "Rename type is None !"

        # fist thing to do is to check if name entry is not empty
        validator = Validator()
        validator.add_validation(self.rename_entry, "Name", Validator.NOT_EMPTY)
        violations = validator.validate()
        if violations:
            self.wm.display_error(title="Invalid name", message="New name cannot be empty")
            return

        # we must check whch entity type we are manipulating
        project = self.core.get_open_project()
        new_name = self.rename_entry.get_text()
        if self.rename_type == MainController.VIEWS_NAME:
            self.core.rename_view( project, self.rename_id, new_name )
        elif self.rename_type == MainController.CONSTRAINTS_NAME:
            self.core.rename_constraints(project, self.rename_id, new_name)
        elif self.rename_type == MainController.LABELS_NAME:
            self.core.rename_labels( project, self.rename_id, new_name )

        # finally, we clear rename text and renamed id/type
        self.rename_entry.set_text("")
        self.rename_container.hide()
        self.rename_id = None
        self.rename_type = None


    def __delete(self, *args):
        tree_selection = self.project_treeview.get_selection()
        model, treeiter = tree_selection.get_selected()
        entity_id, entity_name, *_ = model[treeiter]
        parent_iter = model.iter_parent(treeiter)
        parent = model[parent_iter][1]

        response = self.wm.confirm_action( title="Deletion", message="Are you sure ?" )
        if not response: return

        project = self.core.get_open_project()
        if parent == MainController.VIEWS_NAME:
            self.core.delete_view(project, entity_id)
        elif parent == MainController.CONSTRAINTS_NAME:
            self.core.delete_constraints(project, entity_id)
        elif parent == MainController.LABELS_NAME:
            self.core.delete_labels(project, entity_id)

    def on_close_project(self, *args):
        self.core.close_project()
        self.wm.open_content( "home_content" )


    def __fill_project_liststore(self):
        self.project_treestore.clear()
        project = self.core.get_open_project()

        database_iter = self.project_treestore.append(None, ["", MainController.DATABASE_NAME])
        database = project.get_database()
        self.project_treestore.append(database_iter, [ database.get_id(), database.get_name() ])

        views = project.get_views()
        if views:
            views_iter = self.project_treestore.append(None, ["", MainController.VIEWS_NAME])
            for view in project.get_views():
                self.project_treestore.append(views_iter, [view.get_id(), view.get_name()])

        constraints_iter = self.project_treestore.append(None, ["", "Constraints"])
        for constraint in project.get_constraints():
            self.project_treestore.append(constraints_iter, [ constraint.get_id(), constraint.get_name() ])

        labels_iter = self.project_treestore.append(None, [ "", "Labels" ])
        for labels in project.get_labels():
            self.project_treestore.append(labels_iter, [ labels.get_id(), labels.get_name() ])


    def __fill_database_liststore(self):
        self.database_liststore.clear()
        for database in self.__get_databases():
            self.database_liststore.append([database.get_id(), database.get_name()])


    def __get_databases(self) -> [DatabaseEntity]:
        project = self.core.get_open_project()
        return project.get_all_databases()
    def start(self):
        # return to home content when no project
        if not self.core.has_open_project():
            self.wm.open_content("home_content")

        self.__fill_project_liststore()
        self.__fill_database_liststore()


    def __create_project_details_popup(self):
        rename_item = Gtk.MenuItem(label="Rename")
        rename_item.connect("button-press-event", self.__on_popup_rename_start)

        delete_item = Gtk.MenuItem(label="Delete")
        delete_item.connect("button-press-event", self.__delete)

        menu = Gtk.Menu()
        menu.add(rename_item)
        menu.add(delete_item)
        menu.show_all()
        return menu
from pyclusters.core.core import Core
from pyclusters.core.entities import IdentifierManager, ConstraintsFactory, LabelsFactory, Labels, Constraints, \
    ConstraintsEntity, LabelsEntity
from pyclusters.core.interface import LabelsIOModule, ConstraintsIOModule
from pyclusters.exceptions.core import InvalidFileFormatError, InvalidConstraintsError, InvalidLabelsError
from pyclusters.exceptions.interface import InvalidConfiguationException, ModuleExecutionError
from pyclusters.interface.gtk_utils import ComboBoxUtils, GtkSpinnerManager, OnThread, GtkTextViewManager, OnUIThread
from pyclusters.interface.validators import Validator
from pyclusters.interface.windows_manager import GtkController, WindowsManager

import logging as log

class MainImportController(GtkController):
    def __init__(self, core: Core, wm: WindowsManager):
        self.core = core
        self.wm = wm

        self.labels_modules = {
            module.__name__: module
            for module in self.core.get_modules_of_class(LabelsIOModule)
        }

        self.constraints_module = {
            module.__name__: module
            for module in self.core.get_modules_of_class(ConstraintsIOModule)
        }

    def init_view(self, object_accessor):

        self.import_constraints_radio = object_accessor("main_content_import_constraints_button")
        self.import_labels_radio = object_accessor("main_content_import_labels_button")
        self.import_location_entry = object_accessor("main_content_import_location_entry")
        self.import_modules_liststore = object_accessor("main_content_import_module_liststore")

        self.import_modules_combo = object_accessor("main_content_import_module_combo")
        import_module_details_textview = object_accessor("main_content_import_module_details_textview")
        self.import_module_details_manager = GtkTextViewManager( import_module_details_textview )

        self.import_name_entry = object_accessor("main_content_import_name_entry")
        self.import_id_entry = object_accessor("main_content_import_id_entry")
        self.database_combo = object_accessor("main_content_import_database_combo")
        self.import_spinner = object_accessor("main_content_import_spinner")
        self.import_button = object_accessor("main_content_import_button")

    def start(self):
        self.__project = self.core.get_open_project()

        # init modules
        self.__fill_import_modules()

    def import_constraints_or_labels_changed(self, event):
        try:
            name = self.import_name_entry.get_text()
            self.import_id_entry.set_text(IdentifierManager.create_identifier_by_name(name) if name else "")
        except ValueError:
            self.import_id_entry.set_text("")

    def import_type_changed(self, *args):
        self.__fill_import_modules()

    def import_module_changed(self, *args):
        if ComboBoxUtils.has_active(self.import_modules_combo):
            module_name = ComboBoxUtils.get_active(self.import_modules_combo)[0]
            if self.import_constraints_radio.get_active():
                module_class = self.constraints_module[module_name]
            else:
                module_class = self.labels_modules[module_name]
            module_doc = module_class.__doc__
            if module_doc:
                self.import_module_details_manager.set_text(module_doc)
        else:
            self.import_module_details_manager.set_text("")

    def __fill_import_modules(self):
        # put constraints reader modules when constraint radio is active
        # otherwise put labels modules
        if self.import_constraints_radio.get_active():
            modules_class = self.__get_constraints_modules()
        else:
            modules_class = self.__get_labels_modules()

        self.import_modules_liststore.clear()
        for module in modules_class:
            self.import_modules_liststore.append([module.__name__])
        self.import_modules_combo.set_active(0)

    def search_constraints_or_labels_file_location(self, event):
        filename = self.wm.select_file()
        if filename:
            self.import_location_entry.set_text(filename)

    @OnThread
    def import_constraints_or_labels(self, event):
        # checks that all fields are correctly imported
        if not self.__validate_import_fields():
            return

        try:
            with GtkSpinnerManager.build_with_button(self.import_spinner, self.import_button):
                if self.import_constraints_radio.get_active():
                    self.__import_constraints()
                else:
                    self.__import_labels()
                self.__clear_import_fields()
        except InvalidFileFormatError:
            self.wm.display_error(title="Importing Error", message="Invalid constraints format file")
        except InvalidLabelsError:
            self.wm.display_error(title="Importing Error", message="Labels are incompatible with database")
        except InvalidConstraintsError:
            self.wm.display_error(title="Importing Error", message="Constraints are incompatible with database")
        except InvalidConfiguationException as e:
            self.wm.display_error(title="Module Error", message="Module configuration failed: {}".format(str(e)))
        except ModuleExecutionError as e:
            log.exception(e)
            self.wm.display_error(title=e.get_title(), message=e.get_message())
        except Exception as e:
            log.exception(e)
            self.wm.display_error(title="An Error Occured", message=str(e))


    def __import_labels(self):
        # get and inflating module
        module_name = ComboBoxUtils.get_first_column_of_active(self.import_modules_combo)
        module_class = self.labels_modules[module_name]
        module: LabelsIOModule = module_class()

        # retrieve database
        database_id = ComboBoxUtils.get_first_column_of_active(self.database_combo)

        # read constraints from filename using specified module
        labels_filename = self.import_location_entry.get_text()
        labels : Labels = module.read(labels_filename)
        if not isinstance(labels, Labels):
            raise ModuleExecutionError("Module returned None or invalid data")

        labels_entity: LabelsEntity = LabelsFactory.build(
            id=self.import_id_entry.get_text(),
            name=self.import_name_entry.get_text(),
            labels=labels,
            database_id=database_id
        )

        self.core.add_labels_to_project(self.__project, labels_entity)

        # notify user that everything is okay
        self.wm.display_info(title="Labels imported")



    def __import_constraints(self):
        # get and inflating module
        module_name = ComboBoxUtils.get_active(self.import_modules_combo)[0]
        module_class = self.constraints_module[module_name]
        module: ConstraintsIOModule = module_class()

        # retrieve database
        database_id = ComboBoxUtils.get_first_column_of_active(self.database_combo)

        # read constraints from filename using specified module
        constraints_filename = self.import_location_entry.get_text()
        constraints : Constraints = module.read(constraints_filename)

        constraints_entity : ConstraintsEntity = ConstraintsFactory.build(
            id=self.import_id_entry.get_text(),
            name=self.import_name_entry.get_text(),
            database_id=database_id,
            ml=constraints.get_ml(),
            cl=constraints.get_cl()
        )
        self.core.add_constraints_to_project(self.__project, constraints_entity)

        # notify user that everything is okay
        self.wm.display_info(title="Constraints imported")

    @OnUIThread
    def __clear_import_fields(self):
        self.import_id_entry.set_text("")
        self.import_name_entry.set_text("")
        self.import_location_entry.set_text("")

    def __validate_import_fields(self):
        # validate feilds
        validator = Validator()
        validator.add_validation(self.import_name_entry, "Import name", Validator.NOT_EMPTY)
        validator.add_validation(self.import_id_entry, "Import id", [
            Validator.NOT_EMPTY,
            Validator.custom(
                lambda entry: IdentifierManager.is_valid_identifier(entry.get_text()),
                "Invalid database identifier"
            )
        ])
        validator.add_validation(self.import_location_entry, "Import location", [
            Validator.NOT_EMPTY,
            Validator.PATH_EXISTS,
            Validator.IS_FILE
        ])
        validator.add_validation(self.import_modules_combo, "Module Loader", Validator.COMBO_HAS_CHOICE)
        violations = validator.validate()

        # prevent users if violations
        if violations:
            message = "\n".join(violations)
            self.wm.display_error(message)

        return not violations

    def __get_constraints_modules(self):
        return list(self.constraints_module.values())

    def __get_labels_modules(self):
        return list(self.labels_modules.values())

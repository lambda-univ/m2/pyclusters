from typing import Callable, Union

from gi.repository.Gtk import Entry

from os.path import exists, isfile

from pyclusters.interface.gtk_utils import ComboBoxUtils


class ValidationProperty:
    def __init__(self, property_message, property_validator ):
        self.property_message, self.property_validator = property_message, property_validator

    def valid_entry(self, entry):
        return self.property_validator(entry)

    def get_message(self, property_name):
        return self.property_message.format(property_name)

class Validator:
    NOT_EMPTY = ValidationProperty( "{} cannot be empty", lambda entry: entry.get_text() != '')
    PATH_EXISTS = ValidationProperty( "{} must be a valid path", lambda entry: exists(entry.get_text()) )
    IS_FILE = ValidationProperty( "{} must be a file", lambda entry: isfile(entry.get_text()))
    COMBO_HAS_CHOICE = ValidationProperty( "{} cannot be empty", lambda entry: ComboBoxUtils.has_active(entry))



    @staticmethod
    def check( property ):
        return ValidationProperty( property )

    def __init__(self):
        self.validations = []

    def add_validation( self, entry, property_name, validations ):
        self.validations.append( (entry, property_name, validations) )
        return self

    def validate(self):
        violations = []
        for entry, property_name, validations_list in self.validations:
            if isinstance(validations_list, ValidationProperty):
                validations_list = [validations_list]

            for validation_property in validations_list:
                if not validation_property.valid_entry(entry):
                    violations.append(validation_property.get_message( property_name ))
                    break
        return violations

    @staticmethod
    def custom(validator, message):
        return ValidationProperty(message, validator)

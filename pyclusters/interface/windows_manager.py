from inspect import getmembers, isfunction
from typing import Optional

from pyclusters.core.core import Core

import gi
from pyclusters.core.subject_observer import Observer, Message
from pyclusters.interface.gtk_utils import OnUIThread

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from gi.repository.Gtk import Builder

class GtkController(Observer):
    def init_view(self, object_accessor): pass
    def start(self): pass
    def stop(self): pass

    def notify(self, message):
        if message == Message.PROJECT_UPDATED:
            self.project_updated()

    def project_updated(self): pass


class PyClustersWindow(Gtk.Window):
    def __init__(self):
        super(PyClustersWindow, self).__init__(title="PyClusters")
        self.__container = Gtk.Box()
        self.present_containers = []
        self.connect("delete-event", Gtk.main_quit)


class WindowsManager:
    CONTROLLER_DOWN = 0
    CONTROLLER_UP = 1
    def __init__(self, core : Core, interface_filename : str):
        self.__core = core
        self.__main_content_id: Optional[str] = None
        self.__controllers_by_content_id = {}
        self.__controllers_status = {}
        self.__interface_filename = interface_filename
        self.__builder = Gtk.Builder()
        self.__handlers = {}

        # attributes to manager current displayed window
        self.__running_content = None
        self.__running_content_id = None

        # creates our window
        self.__window = PyClustersWindow()

    def add_window(self, content_id : str, controller_class, is_main : bool = False):
        if is_main:
            self.__main_content_id = content_id

        # creating controller class
        controller = controller_class(self.__core, self)
        self.__core.add_observer( controller )
        self.__controllers_status[controller] = WindowsManager.CONTROLLER_DOWN

        # a content id can be linked with mutilple controllers (for instance with complex interfaces with
        # different semantic parts like Main page
        # it's why we link a content with a list
        if content_id not in self.__controllers_by_content_id:
            self.__controllers_by_content_id[content_id] = []
        self.__controllers_by_content_id[content_id].append( controller )

        # set up handlers with controller
        for method_name, method in getmembers(type(controller), predicate=isfunction):
            if not method_name.startswith("_"):
                print("Binds event {} with method {}.{}".format(method_name, controller, method))
                self.__handlers[method_name] = getattr(controller, method_name)

    def add_style(self, css_filename):
        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        provider.load_from_path( css_filename )
        Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


    def get_object(self, object_id):
        widget = self.__builder.get_object(object_id)
        if widget:
            return widget
        else:
            raise ValueError("No object found with id {}".format(object_id))
    def open_content(self, content_id):
        # stop running window if first window
        if self.__running_content_id is not None:
            self.__running_content_id = None
            self.__running_content.hide()
            self.__running_content = None

        # initiate controller with components when install
        controllers : [GtkController] = self.__controllers_by_content_id[content_id]
        for controller in controllers:
            if self.__controllers_status[controller] == WindowsManager.CONTROLLER_DOWN:
                self.__controllers_status[controller] = WindowsManager.CONTROLLER_UP
                controller.init_view(self.get_object)

        # start controller
        for controller in controllers:
            controller.start()

        # access and displays window
        content = self.__builder.get_object(content_id)
        if content is None:
            raise ValueError("Content with id {} not found !".format(content_id))
        self.__running_content = content
        self.__running_content_id = content_id

        self.__put_content_inside_window(content)

    def start(self):
        # fail when no entry point specified
        if not self.main_defined():
            raise RuntimeError("Undefined main window")

        # building interface from provided interface filename
        # and connect controllers with interface
        self.__builder.add_from_file(self.__interface_filename)
        self.__builder.connect_signals(self.__handlers)

        # open main window
        self.open_content(self.__main_content_id)
        self.__window.show_all()
        Gtk.main()

    def main_defined(self): return self.__main_content_id is not None

    def __get_object_accessor(self):
        return self.__builder.get_object

    def __put_content_inside_window(self, content):
        child = self.__window.get_child()
        if child is not None:
            self.__window.remove(child)
        self.__window.add(content)
        content.show()

    def select_file(self, title = "Select File") -> Optional[str]:
        dialog = Gtk.FileChooserDialog(
            title=title, parent=self.__window, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )


        response = dialog.run()
        filename = None
        if response == Gtk.ResponseType.OK:
            filename = dialog.get_filename()
        dialog.destroy()

        return filename

    @OnUIThread
    def display_info(self, title, message = None):
        dialog = Gtk.MessageDialog(
            transient_for=self.__window,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text=title,
        )
        if message:
            dialog.format_secondary_text(
                message
            )
        dialog.run()
        dialog.destroy()

    @OnUIThread
    def display_error(self, message, title = "An error occurred"):
        dialog = Gtk.MessageDialog(
            transient_for=self.__window,
            flags=0,
            message_type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.CANCEL,
            text=title,
        )
        if message:
            dialog.format_secondary_text(message)
        dialog.run()
        dialog.destroy()

    def get_window(self): return self.__window

    def confirm_action(self, title, message):
        dialog = Gtk.MessageDialog(
            transient_for=self.__window,
            flags=0,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            text=title,
        )
        dialog.format_secondary_text(
            message
        )
        response = dialog.run()
        dialog.destroy()
        return response == Gtk.ResponseType.YES

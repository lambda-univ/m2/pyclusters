from pyclusters.core.subject_observer import Subject, Observer, Message
from pyclusters.core.scanner import ModulesRegister
from pyclusters.core.entities import Project, LabelsEntity, ConstraintsEntity, DatabaseEntity
from pyclusters.core.storage import StorageFacade, DatabaseEntityDescriptor
from pyclusters.exceptions.core import InvalidConstraintsError, InvalidLabelsError
from typing import Optional
import logging as log

class Model:
    def __init__(self):
        self.__current_project : Optional[Project] = None

    def has_project(self) -> bool:
        return self.__current_project is not None

    def open_project(self, project: Project):
        assert type(project) == Project, "Model cannot store open project with type different from Project"
        self.__current_project = project

    def close_project(self): self.__current_project = None

    def get_project(self) -> Project:
        if self.has_project():
            return self.__current_project
        else:
            raise Exception("No current project available")


class Core(Subject):
    def __init__(self, register: ModulesRegister):
        self.__register: ModulesRegister = register
        self.__storage = StorageFacade()
        self.__model = Model()
        self.__observers = []

    def add_observer(self, observer: Observer):
        """Adds an observer.

        :param observer: Observer that will be added in Core.
        :return:
        """
        if observer not in self.__observers:
            log.debug("Observer registered: {}".format(observer))
            self.__observers.append(observer)

    def notify_all(self, message):
        """Notifies all registered observers.

        Observers must implement Observer class to be notified.

        :param message: Message that will be sent at all registered observers.
        """
        for observer in self.__observers:
            observer.notify(message)

    def open_project_with_id(self, project_id):
        """Opens project that has given id.

        :param project_id: Project's identifier that will be added to project.
        """
        log.info("Opening project {}".format(project_id))
        project = self.__storage.find_project_by_id( project_id )
        self.__model.open_project( project )


    def close_project(self):
        """Close current opened project.
        """
        self.__model.close_project()

    def has_open_project(self) -> bool: return self.__model.has_project()

    def get_open_project(self) -> Project:
        """Returns the current open project
        :return: Current open project.
        """
        return self.__model.get_project()

    def get_modules_of_class(self, module_type):
        """Returns all modules of a given module type.

        :param module_type:
        :return: All modules of a given module type.
        """
        return self.__register.find_modules_of_class( module_type )


    def import_database(self, database : DatabaseEntity):
        """
        Imports a database.

        :raises ConflictException: Database with specified id already exists.
        """
        self.__storage.import_database( database )

    def get_all_databases(self) -> [DatabaseEntityDescriptor]:
        """Returns all stored databases descriptions.

        As a database can be large, load all databases could be very long.
        So on, returned databases are data-free, as a database descriptor.

        :return: All stored databases descriptions.
        """
        return self.__storage.find_all_database()

    def get_database_by_id(self, database_id):
        """Returns database that corresponding with database id.

        :param database_id: Database's id you want to load.
        :return: Database that corresponding with database id.
        """
        return self.__storage.find_database_by_id( database_id )

    def create_project(self, project : Project):
        """Create a project.

        :param project: Project that will be created.
        """
        self.__storage.add_project(project)

    def get_all_projects(self):
        """Returns all projects descriptions.

        As a project can contains very large datasets (e.g. labels, constraints),
        returned projects are data-free.

        :return: All projects description;
        """
        return self.__storage.find_all_projects()

    def add_constraints_to_project(self, project : Project, constraints : ConstraintsEntity):
        """Adds constraints in given project and registered all registered observers.

        Insert constraints in project and store them locally.

        :param project: Project in which constraints are added.
        :param constraints: Constraints that will be stored and added in project.

        :raises InvalidConstraintsError: Constraints aren't valid regarding on database.
        """
        project.add_constraints(constraints)
        self.__storage.add_constraints( project, constraints )
        self.notify_all(Message.PROJECT_UPDATED)


    def add_labels_to_project(self, project : Project, labels : LabelsEntity):
        """Adds labels in given project and notifies all registered observers.

        :param project: Project in which labels are added.
        :param labels: Labels that will be stored and added in project.

        :raises InvalidLabelsError: Errors aren't valid regarding on database.
        """
        project.add_labels_set(labels)
        self.__storage.add_labels_set( project, labels )
        self.notify_all(Message.PROJECT_UPDATED)

    def add_view(self, project : Project, view : DatabaseEntity):
        """Adds and stores view in given project and notifies all registered observers.

        :param project: Project in which view will be added.
        :param view: View that will be added.
        """
        project.add_view(view)
        self.__storage.add_view(project, view)
        self.notify_all(Message.PROJECT_UPDATED)

    def rename_view(self, project : Project, view_id : str, new_name :str):
        """Renames a view with the provided new name and notifies all registered observers.

        :param project: Project in which views will be added.
        :param view_id: View's identifier which will be renamed.
        :param new_name: New view's name.

        :raises DatabaseNotFoundException: Database not found.
        """
        view = project.get_database_or_view_by_id(view_id)
        view.set_name( new_name )
        self.__storage.update_view( project, view )
        self.notify_all(Message.PROJECT_UPDATED)

    def rename_constraints(self, project : Project, constraints_id : str, new_name : str):
        """Renames constraints with the provided new name and notifies all registered observers.

        :param project: Project in which views will be added.
        :param constraints_id: Constraints identifier which will be renamed.
        :param new_name: New constraints name.

        :raises ConstraintsNotFoundException: Constraints not found.
        """
        constraints = project.get_constraints_by_id(constraints_id)
        constraints.set_name(new_name)
        self.__storage.update_constraints( project, constraints )
        self.notify_all(Message.PROJECT_UPDATED)

    def rename_labels(self, project : Project, labels_id : str, new_name : str):
        """Renames labels with the provided new name and notifies all registered observers.

        :param project: Project in which views will be added.
        :param labels_id: Labels identifier which will be renamed.
        :param new_name: New constraints name.

        :raises LabelsNotFoundException: Labels not found.
        """
        labels = project.get_labels_by_id(labels_id)
        labels.set_name( new_name )
        self.__storage.update_labels( project, labels )
        self.notify_all(Message.PROJECT_UPDATED)


    def delete_view(self, project : Project, view_id : str):
        """Deletes view in the specified project and notifies registered observers.

        :param project: Project which from view will be deleted.
        :param view_id: View that will be deleted.
        """
        view = project.get_database_or_view_by_id(view_id)
        project.delete_view( view )
        self.__storage.delete_view(  project, view )
        self.notify_all(Message.PROJECT_UPDATED)

    def delete_constraints(self, project: Project, constraints_id: str):
        """Deletes constraints in the specified project and notifies registered observers.

        :param project: Project which from view will be deleted.
        :param constraints_id: Constraints that will be deleted.
        """
        constraints = project.get_constraints_by_id(constraints_id)
        project.delete_constraints(constraints)
        self.__storage.delete_constraints(project, constraints)
        self.notify_all(Message.PROJECT_UPDATED)

    def delete_labels(self, project: Project, labels_id: str):
        """Deletes labels in the specified project and notifies registered observers.

        :param project: Project which from labels will be deleted.
        :param labels_id: View that will be deleted.
        """
        labels = project.get_labels_by_id(labels_id)
        project.delete_labels(labels)
        self.__storage.delete_labels(project, labels)
        self.notify_all(Message.PROJECT_UPDATED)

    def delete_project(self, project_id):
        """Deletes project and all related labels, constraints and views."""
        self.__storage.delete_project(project_id)
        self.notify_all(Message.PROJECT_DELETED)

    def delete_database(self, database_id):
        """Delete database and all related project"""
        # deletes all project related with database
        for project_descriptor in self.__storage.find_all_projects():
            if project_descriptor.get_database_id() == database_id:
                project = self.__storage.find_project_by_id(project_descriptor.get_id())
                self.__storage.delete_project(project)

        # delete database itself
        self.__storage.delete_database(database_id)
        self.notify_all(Message.DATABASE_DELETED)



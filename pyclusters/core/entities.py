import numpy as np
import regex as re
from typing import Optional, Union

from pyclusters.exceptions.core import DatabaseNotFoundException, ConstraintsNotFoundException, LabelsNotFoundException, \
    ConflictException, InvalidLabelsError


class IdentifierCreationException(ValueError):
    """Raised when id creation failed."""

    def __init__(self, message=None):
        ValueError.__init__(self, message)


class IdentifierManager:
    """Identifier Manager handles id creation and validation.
    """
    VALID_IDENTIFIER_REGEX = "^[a-z0-9][a-z0-9\-\_]*$"
    FORMAT_IDENTIFIER_REGEX = "[^a-z0-9]+"

    @staticmethod
    def is_valid_identifier(identifier: str):
        """Returns True if given id is valid, False otherwise.

        An id is valid if matchs ^[a-z0-9][a-z0-9-_]*$ regex.

        Examples:
        >>> id = "a_valid_identifier"
        >>> IdentifierManager.is_valid_identifier(id)
        True
        >>> id = "an_invalid_identifier"
        >>> IdentifierManager.is_valid_identifier(id)
        False

        :param identifier The identifier that will be tested.
        :return True if given id is valid, False otherwise.
        """
        # an id must be a str type
        if type(identifier) != str: return False

        return re.match(IdentifierManager.VALID_IDENTIFIER_REGEX, identifier)

    @staticmethod
    def create_identifier_by_name(name: str):
        """Returns an identifier from a string.

        To convert a string into a valid identifier, invalid characters are removed.

        :param name: Str that will be converted into a valid identifier.

        :raises IdentifierCreationException: When given name cannot be converted into identifier.
        """
        if type(name) != str: raise IdentifierCreationException("Invalid name identifier")
        id = re.sub(IdentifierManager.FORMAT_IDENTIFIER_REGEX, '', name.lower().replace(" ", ""))

        # checks if computed id contains symbols
        # if not, given name was invalid
        if id != "":
            return id
        else:
            raise IdentifierCreationException("Invalid given name")


class Entity:
    """Represents an object which can be identified formally with an identifier.

    It also contains a name a description.

    """

    def __init__(self, id: str, name: str, description: str):
        self.__id = id
        self.__name = name
        self.__description = description

    def get_id(self): return self.__id

    def get_name(self): return self.__name

    def get_description(self): return self.__description

    def set_name(self, name): self.__name = name

    def set_dscription(self, description): self.__description = des


class Labels:
    """Represents a Labels set.
    """

    def __init__(self, labels: np.ndarray):
        self.labels = labels

    def get_labels(self) -> np.ndarray: return self.labels

    def nb_instances(self):
        """Returns labels instances number."""
        return self.labels.shape[0]

    def __iter__(self):
        return iter(self.labels)


class LabelsEntity(Entity):
    """Represents a labels set that can be identified.

    """

    def __init__(self, id: str, name: str, description: str, labels: Labels, database_id: str):
        Entity.__init__(self, id, name, description)
        self.__database_id = database_id
        self.__labels = labels

    def get_labels(self) -> Labels:
        """Returns labels as a Labels object."""
        return self.__labels

    def get_data(self) -> np.ndarray:
        """Returns all labels as a numpy array."""
        return self.__labels.get_labels()

    def nb_instances(self):
        """Returns labels instances number."""
        return self.__labels.nb_instances()

    def __iter__(self):
        return iter(self.__labels)

    def get_database_id(self) -> str:
        """Returns database identifier to which labels are related."""
        return self.__database_id


class LabelsFactory:
    """Labels factory used to create Labels and LabelsEntity object."""

    @staticmethod
    def build(id: str, name: str, labels, database_id: str, description=""):
        """Builds and returns a new LabelsEntity object.

        :param id: Labels identifier.
        :param name: Labels name.
        :param labels: Labels data.
        :param database_id: Database's id to which are related with.
        :param description: Labels description.

        :return: LabelsEntity object.

        :raises ValueError: If given data are invalid or id isn't valid.
        """
        # convert labels in numpy array
        if not isinstance(labels, Labels):
            labels = LabelsFactory.build_from_data(labels)

        # checks that id, name and description are given and valid.
        if type(id) != str: raise ValueError("Id must be str")
        if type(name) != str: raise ValueError("Name must be str")
        if type(description) != str: raise ValueError("Description must be str")

        # checks that provided identifier is valid
        if not IdentifierManager.is_valid_identifier(id):
            raise ValueError("Provided id isn't valid")

        # checks if labels match
        return LabelsEntity(
            id=id,
            name=name,
            database_id=database_id,
            labels=labels,
            description=description
        )

    @staticmethod
    def build_from_data(labels):
        """Returns a new Labels object.

        :param labels: Labels data.
        :return: New labels instance.

        :raises ValueError: When isn't a list or invalid labels format.
        """
        labels = np.asarray(labels)
        if len(labels.shape) != 1:
            raise ValueError(f"Invalid labels format: Expected one dimensional array, got {labels.shape}")
        return Labels(labels)


class Constraints:
    """Constraints class represent a set a constraints.
    """

    def __init__(self, ml: np.ndarray, cl: np.ndarray):
        self.__ml = ml
        self.__cl = cl

    def get_ml(self) -> np.ndarray:
        """Returns Must-Link constraints."""
        return self.__ml

    def get_cl(self) -> np.ndarray:
        """Returns Cannot-Link constraints."""
        return self.__cl


class ConstraintsEntity(Entity):
    """Represents an identifiable constraints object."""
    def __init__(self, id: str, name: str, description: str, database_id: str, constraints: Constraints):
        Entity.__init__(self, id, name, description)
        self.__database_id = database_id
        self.__constraints = constraints

    def get_constraints(self) -> Constraints:
        """Returns constraints object."""
        return self.__constraints

    def get_ml(self) -> np.ndarray:
        """Returns Must-Link constraints"""
        return self.__constraints.get_ml()

    def get_cl(self) -> np.ndarray:
        """Returns Cannot-Link constraints."""
        return self.__constraints.get_cl()

    def get_database_id(self) -> str:
        """Returns database's id from which constraints are related with."""
        return self.__database_id


class ConstraintsFactory:
    """Constraints factory class used to build constraints."""
    @staticmethod
    def build_from_data(ml: [(int, int)], cl: [(int, int)]) -> Constraints:
        """
        Builds a constraints object from given Must-Link and Cannot-Link constraints.

        :param ml: Must-Link constraints.
        :param cl: Cannot-Link constraints.
        :return: a new Constraints object.

        :raises ValueError: When given constraints are invalid.
        """
        # checks ml and cl constraints. They must have a valid type
        # a must-link must be of type [(int, int)]
        for constraint in ml:
            if len(constraint) != 2:
                raise ValueError("Invalid must-link constraints")

        # a cannot-link constraint must be of type [(int, int)]
        for constraint in cl:
            if len(constraint) != 2:
                raise ValueError("Invalid cannot-link constraints")

        return Constraints(
            ml=np.asarray(ml),
            cl=np.asarray(cl)
        )

    @staticmethod
    def build(id, name, database_id: str, ml, cl, description="") -> ConstraintsEntity:
        """
        Returns constraints entity object.

        :param id: Constraints identifier.
        :param name: Constraints name.
        :param database_id: Database's id from which constrains are related with.
        :param ml: Must-Link constraints.
        :param cl: Cannot-Link constraints.
        :param description: Constraints description.
        :return: Constraints entity object.
        """
        return ConstraintsEntity(
            id=id,
            name=name,
            database_id=database_id,
            description=description,
            constraints=ConstraintsFactory.build_from_data(
                ml=ml,
                cl=cl
            )
        )


class Database:
    """Database object represents a two-dimensional array."""

    def __init__(self, data: np.ndarray, headers: [str] = []):
        self.__data: np.ndarray = data
        self.__headers = headers

    def __len__(self): return len(self.__data)

    def has_headers(self) -> bool:
        """Return True if database contains header, False otherwise."""
        return len(self.__headers) == 0

    def shape(self):
        """Returns database's shape as a tuple (N, K) where N is instances number and K the instance dimension"""
        return self.__data.shape

    def get_data(self) -> np.ndarray:
        """Returns data."""
        return self.__data

    def get_headers(self) -> [str]:
        """Returns headers as a list of str, or empty list if no headers."""
        return self.__headers

    def nb_instances(self):
        """Returns instances number."""
        return len(self)

    def instance_dimension(self):
        """Returns instance dimension."""
        return self.shape()[1]

    def get_transposed(self):
        """Returns transposed data."""
        return self.__data.transpose()


class DatabaseEntity(Entity):
    """Database is an object which can be identified."""
    def __init__(self, id: str, name: str, description: str, database: Database):
        Entity.__init__(self, id, name, description)
        self.__database = database

    def shape(self) -> (int, int):
        """Returns database shape."""
        return self.__database.shape()

    def __len__(self):
        return len(self.__database)

    def __str__(self):
        return str(self.__dict__)

    def has_headers(self) -> bool:
        """Returns True if database has headers, False otherwise."""
        return self.__database.has_headers()

    def get_data(self) -> np.ndarray:
        """Returns data as a two-dimensional numpy array."""
        return self.__database.get_data()

    def get_database(self) -> Database:
        """Returns database object."""
        return self.__database

    def get_headers(self) -> [str]:
        """Returns headers as a list of str or an empty list if empty."""
        return self.__database.get_headers()

    def nb_instances(self):
        """Returns instances number."""
        return len(self.__database)

    def check_constraints_are_valid(self, constraints: ConstraintsEntity) -> bool:
        """Returns True if given constraints are valid regarding on database, False otherwise."""
        nb_instances = self.nb_instances()
        for x, y in constraints.get_ml():
            if x < 0 or nb_instances <= x or y < 0 or nb_instances <= y:
                return False

        for x, y in constraints.get_cl():
            if x < 0 or nb_instances <= x or y < 0 or nb_instances <= y:
                return False

        return True

    def check_labels_are_valid(self, labels_set: LabelsEntity):
        """Returns True if given labels are valid regarding on database, False otherwise. """
        return len(labels_set.get_data()) == self.nb_instances()


class DatabaseFactory:
    @staticmethod
    def build_from_data(data: [[Union[int, float, np.ndarray]]], headers: [str] = []) -> Database:
        """
        Builds and returns a new database object.

        :param data: Database's data
        :param headers: Database's headers.
        :return: A new database object.

        :raises ValueError: If given data or headers are invalid.
        """
        # convert data from raw type to numpy type
        data = np.asarray(data)
        if len(data.shape) != 2:
            raise ValueError("Invalid data format, expected {}, got {}".format("(N, K)", data.shape))

        # ensures that headers match number of columns
        if len(headers) != 0 and len(headers) != data.shape[1]:
            raise ValueError("Headers number must be valid: got {}".format(headers))

        return Database(
            data=data,
            headers=headers
        )

    @staticmethod
    def build(id: str, name: str, data, description: str = "", headers: [str] = []) -> DatabaseEntity:
        '''
        Build a database object from a given dataset.

        :param id: Database unique identifiant.
        :param name: Database name
        :param data: Database itself. Expected data's dimensions are (N, K) where N is instances number
        and K instance dimension.
        :param description: Database description. If omitted, default value is an empty string.
        :param headers: Headers used to describe each data's column. If omitted, default value is no headers.

        :raise ValueError: When invalid data type given, invalid provided data shape or invalid database identifier.

        :return: Built database object.
        '''

        # checks input type
        if type(id) != str: raise ValueError("Database identifier must be str")
        if type(name) != str: raise ValueError("Database name must be str")
        if type(description) != str: raise ValueError("Database description must be str")

        # ensures that values are valid typ
        assert type(id) == str, "Database identifier must be str"
        assert type(name) == str, "Database name must be str"
        assert type(description) == str, "Database description must be str"

        if id == "": raise ValueError("Database's id cannot be empty")
        if name == "": raise ValueError("Database's name cannot be empty")
        if not IdentifierManager.is_valid_identifier(id): raise ValueError("Invalid database identifier")

        if not isinstance(data, Database):
            data = DatabaseFactory.build_from_data(data, headers)

        return DatabaseEntity(
            id=id,
            name=name,
            description=description,
            database=data
        )


class Project:
    """Project represents a PyClusters project.
    """

    def __init__(
            self,
            id: str,
            name: str,
            description: str,
            database: DatabaseEntity,
            labels: [LabelsEntity],
            constraints: [ConstraintsEntity],
            views: [DatabaseEntity]
    ):
        self.id = id
        self.name = name
        self.description = description
        self.database: DatabaseEntity = database
        self.labels = {labels_set.get_id(): labels_set for labels_set in labels}
        self.constraints = {c.get_id(): c for c in constraints}
        self.views = {view.get_id(): view for view in views}

    def add_constraints(self, constraints: ConstraintsEntity):
        """Adds constraints in project.

        Given constraints must be valid regarding on related database.

        :param constraints:
        :return:
        """
        # handle id conflict
        if constraints.get_id() in self.constraints:
            raise ConflictException("Constraints with specified id already exists")

        # handle invalid constraints
        database: DatabaseEntity = self.get_database_or_view_by_id(constraints.get_database_id())
        if not database.check_constraints_are_valid(constraints):
            raise ValueError("Constraints are not valid !")

        self.constraints[constraints.get_id()] = constraints

    def add_labels_set(self, labels: LabelsEntity):
        """Adds labels in project.

        :param labels: Labels to be inserted in project.

        :raises ConflictException: Labels with given id already exists.
        :raises InvalidLabelsError: Labels is not valid regarding related database.
        :raises DatabaseNotFoundException: Related database not found.
        """
        # handle id conflict
        if labels.get_id() in self.labels:
            raise ConflictException("Labels with specified id already exists")

        # handle invalid labels related with database
        database = self.get_database_or_view_by_id(labels.get_database_id())
        if not database.check_labels_are_valid(labels):
            raise InvalidLabelsError("Labels are not valid !")

        self.labels[labels.get_id()] = labels

    def add_view(self, view: DatabaseEntity):
        """Adds view in project.

        :param view: View to be inserted in project.
        :raises ConflictException: View with
        """
        if view.get_id() in self.views:
            raise ConflictException("View with specified id already exists")
        self.views[view.get_id()] = view

    def database_shape(self) -> (int, int):
        return self.database.shape()

    def get_constraints_by_id(self, constraints_id) -> ConstraintsEntity:
        if constraints_id in self.constraints:
            return self.constraints[constraints_id]
        else:
            raise ConstraintsNotFoundException("Constraints with id {} not found !".format(constraints_id))

    def get_database_or_view_by_id(self, database_id) -> DatabaseEntity:
        """ Returns the database corresponding to the given identifier.

        :param database_id: Database id.
        :return: Database matched with given database id.
        :raises DatabaseNotFoundException: When no one database corresponding with given id.
        """
        if self.database.get_id() == database_id:
            return self.database
        else:
            if database_id in self.views:
                return self.views[database_id]
            DatabaseNotFoundException("Database not found !")

    def get_labels_by_id(self, labels_id) -> LabelsEntity:
        """ Returns labels corresponding to the given identifier.

        :param labels_id: Labels id.
        :return: Labels matched with given database id.
        :raises LabelsNotFoundException: When no one labels corresponding with given id.
        """
        if labels_id in self.labels:
            return self.labels[labels_id]
        else:
            raise LabelsNotFoundException("Labels not found")

    def get_view_by_id(self, view_id) -> DatabaseEntity:
        """ Returns view corresponding to the given identifier.

        :param view_id: Labels id.
        :return: View matched with given database id.
        :raises DatabaseNotFoundException: When no one labels corresponding with given id.
        """
        if view_id in self.views:
            return self.views[view_id]
        raise DatabaseNotFoundException("Views with id {} not found !".format(view_id))

    def get_constraints_related_with_database(self, database_id: str) -> [ConstraintsEntity]:

        return [
            constraints
            for constraints in self.get_constraints()
            if constraints.get_database_id() == database_id
        ]

    def get_labels_related_with_database(self, database_id: str) -> [LabelsEntity]:
        return [
            labels
            for labels in self.get_labels()
            if labels.get_database_id() == database_id
        ]

    def get_all_databases(self) -> [DatabaseEntity]:
        databases = [self.database]
        for view in self.views.values():
            databases.append(view)
        return databases

    def get_database(self) -> DatabaseEntity:
        return self.database

    def get_description(self) -> str:
        return self.description

    def get_constraints(self) -> [ConstraintsEntity]:
        return list(self.constraints.values())

    def get_id(self) -> str:
        return self.id

    def get_name(self) -> str:
        return self.name

    def get_labels(self) -> [LabelsEntity]:
        return list(self.labels.values())

    def get_views(self):
        return list(self.views.values())

    def delete_view(self, view: DatabaseEntity):
        if view.get_id() in self.views:
            del self.views[view.get_id()]

    def delete_constraints(self, constraints: ConstraintsEntity):
        if constraints.get_id() in self.constraints:
            del self.constraints[constraints.get_id()]

    def delete_labels(self, labels: LabelsEntity):
        if labels.get_id() in self.labels:
            del self.labels[labels.get_id()]


class ProjectFactory:
    @staticmethod
    def build(id: str, name: str, description: str, database: DatabaseEntity) -> Project:
        '''
        Build a project object with given elements.

        :param id: Project's identifier.
        :param name: Project's name
        :param description: Projects' description
        :param database: Project's database

        :return: Project object.
        '''
        # checks input type
        if type(id) != str: raise ValueError("Project's id must be str")
        if type(name) != str: raise ValueError("Project's name must be str")
        if type(description) != str: raise ValueError("Project's description must be str")
        if type(database) != DatabaseEntity: raise ValueError("Project's database must be Database object")

        if id == "": raise ValueError("Project's identifier cannot be empty")
        if name == "": raise ValueError("Project's name cannot be empty")

        # build project
        return Project(
            id=id,
            name=name,
            description=description,
            database=database,
            labels=[],
            constraints=[],
            views=[]
        )


class Clustering:
    def __init__(self, database: DatabaseEntity, constraints: Optional[ConstraintsEntity], labels_set: LabelsEntity):
        self.__database = database
        self.__constaints = constraints
        self.__labels_set = labels_set

from typing import Optional

from matplotlib.figure import Figure
from pyclusters.core.entities import LabelsEntity, ConstraintsEntity, DatabaseEntity, Database, Constraints, Labels
from abc import abstractmethod, ABC

from pyclusters.exceptions.interface import InvalidConfiguationException


class Range:
    MIN_VALUE = -9999999999
    MAX_VALUE = 9999999999

    @staticmethod
    def build(min=MIN_VALUE, max=MAX_VALUE, step=1, default=None):
        # checks parameters type
        for value in [min, max, step, default]:
            if value and not Range.__valid_type(value):
                raise InvalidConfiguationException("Range parameters must be number, got {}".format(type(value)))

        # try to define a default value from value
        default_value = 0
        if default:
            default_value = default
        elif min != Range.MIN_VALUE:
            default_value = min
        elif max != Range.MAX_VALUE:
            default_value = max

        return Range(min, max, step, default_value)

    @staticmethod
    def __valid_type(value):
        value_type = type(value)
        return value_type in [int, float]

    def __init__(self, min=MIN_VALUE, max=MAX_VALUE, step=1, default=None):
        self.min = min
        self.max = max
        self.step = step
        self.default = default


class Choices:
    @staticmethod
    def build(choices, default_index=None):
        # ensures that at least one result is present
        if len(choices) == 0:
            raise InvalidConfiguationException("Cannot create choices parameter: At least one choice is required")

        # by default, first choices is active
        if default_index is None:
            default_index = 0
        return Choices(choices, default_index=default_index)

    def __init__(self, choices, default_index):
        self.choices = choices
        self.default_index = default_index

    def __iter__(self):
        return iter(self.choices)

    def default_value(self):
        return self.choices[self.default_index]


class Confirmation:

    @staticmethod
    def build(default=False):
        if type(default) != bool:
            raise InvalidConfiguationException("Default Confirmation value must be bool")
        return Confirmation(default)

    def __init__(self, default=False):
        self.value = default

    def default_value(self): return self.value


class Entry:
    @staticmethod
    def build(default=""):
        if type(default) != str:
            raise InvalidConfiguationException("Default entry value must be str")
        return Entry(default)

    def __init__(self, default):
        self.value = default

    def default_value(self): return self.value


class Parameter:
    def __init__(self, name, property):
        self.name = name
        self.property = property

    def get_name(self): return self.name

    def get_property(self): return self.property


class ParametersBag:

    @staticmethod
    def empty():
        return ParametersBag()

    def __init__(self):
        self.__parameters_name = []
        self.ranges: [Parameter] = []
        self.choices: [Parameter] = []
        self.confirmation: [Parameter] = []
        self.entries: [Parameter] = []
        self.nb_parameters = 0

    def add(self, name, property):
        if isinstance(property, Range):
            self.add_range(name, property)
        elif isinstance(property, Choices):
            self.add_choices(name, property)
        elif isinstance(property, Confirmation):
            self.add_confirmation(name, property)
        elif isinstance(property, Entry):
            self.add_entry(name, property)
        else:
            raise InvalidConfiguationException("Cannot create field with unkown type: got {}".format(type(property)))

    def add_range(self, name, property: Range):
        if type(property) != Range:
            raise InvalidConfiguationException(
                "Cannot create range property with an non range object, Check Range.build")
        if name in self.__parameters_name:
            raise InvalidConfiguationException("Parameter name {} already exists".format(name))
        self.__parameters_name.append(name)
        self.ranges.append(Parameter(name, property))
        self.nb_parameters += 1

    def add_choices(self, name, property: Choices):
        if type(property) != Choices:
            raise InvalidConfiguationException(
                "Cannot create choices property with an non choices object, Check Choices.build"
            )
        if name in self.__parameters_name:
            raise InvalidConfiguationException("Parameter name {} already exists".format(name))
        self.__parameters_name.append(name)
        self.choices.append(Parameter(name, property))
        self.nb_parameters += 1

    def add_confirmation(self, name, confirmation: Confirmation):
        if not isinstance(confirmation, Confirmation):
            raise InvalidConfiguationException(
                "Cannot create Confirmation property with an non Confirmation object, Check Confirmation.build"
            )
        if name in self.__parameters_name:
            raise InvalidConfiguationException("Parameter name {} already exists".format(name))
        self.__parameters_name.append(name)
        self.confirmation.append(Parameter(name, confirmation))
        self.nb_parameters += 1

    def add_entry(self, name, entry: Entry):
        if not isinstance(entry, Entry):
            raise InvalidConfiguationException(
                "Cannot create Entry property with an non Entry object, Check Entry.build"
            )
        if name in self.__parameters_name:
            raise InvalidConfiguationException("Parameter name {} already exists".format(name))

        self.__parameters_name.append(name)
        self.entries.append(Parameter(name, entry))
        self.nb_parameters += 1

    def has_parameters(self):
        return self.nb_parameters != 0

    def get_ranges(self) -> [Parameter]:
        return self.ranges

    def get_choices(self) -> [Parameter]:
        return self.choices

    def get_confirmation(self) -> [Parameter]:
        return self.confirmation

    def get_entries(self) -> [Parameter]:
        return self.entries


class Configurable:

    def require(self) -> ParametersBag: return ParametersBag.empty()


class LabelsIOModule(Configurable):

    @abstractmethod
    def write(self, filename: str, labels: Labels): pass

    @abstractmethod
    def read(self, filename: str) -> Labels: pass


class ConstraintsIOModule(Configurable):

    @abstractmethod
    def write(self, filename: str, constraints: Constraints): pass

    @abstractmethod
    def read(self, filename: str) -> Constraints: pass


class DatabaseIOModule(Configurable):

    @abstractmethod
    def write(self, filename: str, data: Database): pass

    @abstractmethod
    def read(self, filename: str) -> Database: pass


class ClusteringAlgorithmModule(Configurable):

    @abstractmethod
    def predict(self, database: Database, constraints: Constraints, parameters) -> Labels: ...


class DatabaseVisualizerModule(Configurable):

    @abstractmethod
    def visualize_database(self, database: Database, parameters): ...


class ClusteringVisualizerModule(Configurable):

    @abstractmethod
    def visualize_clustering(self, database: Database, labels: Labels, parameters) -> Figure: ...


class DatabaseTransformerModule(Configurable):

    @abstractmethod
    def transform(self, database: Database, parameters) -> Database: ...


class OutputStream:
    @abstractmethod
    def clear(self): ...

    @abstractmethod
    def print(self, message): ...

    @abstractmethod
    def println(self, message): ...


class GenericModule(Configurable):

    @abstractmethod
    def run(
            self,
            printer: OutputStream,
            database: Optional[Database],
            constraints: Optional[Constraints],
            labels: Optional[Labels],
            parameters
    ): ...


class ComparatorModule(Configurable):
    @abstractmethod
    def compare(self, printer: OutputStream, first_labels: Labels, second_labels: Labels, parameters): ...

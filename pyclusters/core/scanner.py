from pyclusters.core.interface import *

from inspect import isclass
from inspect import getmembers

from importlib import import_module

from os import walk
from os.path import join

from regex import match

import logging as log


class ModulesRegister:
    """
    ModulesRegister is modules holder, which contains all
    modules loaded from modules directory by ModulesScanner.
    """
    def __init__(self):
        self.types = [
            LabelsIOModule,
            ConstraintsIOModule,
            DatabaseIOModule,
            DatabaseVisualizerModule,
            ClusteringAlgorithmModule,
            ClusteringVisualizerModule,
            GenericModule,
            DatabaseTransformerModule,
            ComparatorModule
        ]
        self.modules = {t: set() for t in self.types}
        self.module_by_name = {}

    def register(self, module_type, modules_class):
        """
        Registers modules which inherits from module type.
        
        :param module_type: Module type of which inherits all modules class.
        :param modules_class: Modules which inherits from Module type.
        """
        for cls in modules_class:
            # check configurable
            if not self.__filter(cls):
                continue

            key = (module_type, cls.__name__)
            if cls.__name__ not in self.module_by_name:
                self.module_by_name[key] = cls
                self.modules[module_type].add(cls)
            else:
                existing_module = self.module_by_name[key]
                new_module = cls
                log.warning("Conflict between two modules: Same name used on {} and {}".format(
                    existing_module.__module__, new_module.__module__
                ))

    def get_search_types(self):
        """
        Returns searched modules type.
        :return: Searched modules type.
        """
        return self.types

    def find_modules_of_class(self, module_class):
        """
        Returns all registered subclasses of provided module class.

        :param module_class: Search modules type.
        :return: All registered subclasses of provided module class.
        """
        assert module_class in self.types, "Cannot find modules of unknown class"
        return self.modules[ module_class ]

    def __filter(self, cls):
        try:
            if issubclass(cls, Configurable):
                cls_instance = cls()
                cls_instance.require()
        except Exception as e:
            log.warning("{} object 's require() method is not well configured: {}".format(
                cls.__name__,
                e
            ))
            return False
        return True

    def __getitem__(self, value):
        return self.modules[value]

    def __str__(self):
        return str(self.modules)




PYTHON_FILE_REGEX = ".*.py$"


def is_python_file(filename):
    return match(PYTHON_FILE_REGEX, filename)

class ModulesScanner:
    """
    Modules Scanner used to scan a folder recursively and loads all modules.
    """
    def __init__(self, lib_path: str, lib_name: str):
        self.lib_path = lib_path
        self.register: ModulesRegister = ModulesRegister()
        self.lib_name = lib_name

    def load_modules_from_module_file(self, module_name: str, register: ModulesRegister):
        module = import_module(module_name)
        for search_type in register.types:
            match_subclass_type = lambda object: isclass(object) and issubclass(object,
                                                                                search_type) and object != search_type
            object_types = [object_type for object_name, object_type in getmembers(module, match_subclass_type)]
            log.debug("Register {} of type {}".format(search_type, object_types))
            register.register(search_type, object_types)

    def scan(self, relative_modules_path):
        """
        Starts scan recursively inside relative module path.

        :param relative_modules_path: Path where scan starts.
        """
        absolute_modules_path = join(self.lib_path, relative_modules_path)
        log.debug("Scanning {}".format(absolute_modules_path))
        for root, _, files in walk(absolute_modules_path):
            for file in files:
                if is_python_file(file):

                    # convert python file location into loadable module
                    filename_no_extension = file.replace(".py", "")
                    relative_file_location = root.replace(self.lib_path, "").strip()
                    if relative_file_location.startswith("/"): relative_file_location = relative_file_location[1:]
                    if relative_file_location.endswith("/"): relative_file_location = relative_file_location[:-1]
                    relative_file_location = relative_file_location.replace("/", ".")
                    module_path = ".".join([self.lib_name, relative_file_location, filename_no_extension])

                    # load and register class
                    log.debug("Reading " + module_path)
                    self.load_modules_from_module_file(module_path, self.register)

    def get_register(self) -> ModulesRegister:
        return self.register

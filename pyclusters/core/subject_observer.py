from abc import abstractmethod


class Observer:
    @abstractmethod
    def notify(self, message): ...


class Subject:
    @abstractmethod
    def add_observer(self, observer: Observer): ...

    @abstractmethod
    def notify_all(self, message): ...

class Message:
    DATABASE_DELETED = 2
    PROJECT_DELETED = 1
    PROJECT_UPDATED = 0
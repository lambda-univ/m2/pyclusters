from pyclusters.core.core import Core
from pyclusters.core.scanner import ModulesScanner, ModulesRegister
from pyclusters.interface.controllers.home.database_import_controller import ImportDatabaseController
from pyclusters.interface.controllers.home.home_controller import HomeController
from pyclusters.interface.controllers.home.project_creation_controller import ProjectCreationController
from pyclusters.interface.controllers.main.algorithm_controller import MainAlgorithmController
from pyclusters.interface.controllers.main.comparators_controller import MainComparatorController
from pyclusters.interface.controllers.main.generic_controller import MainGenericController
from pyclusters.interface.controllers.main.import_controller import MainImportController
from pyclusters.interface.controllers.main.main_controller import MainController
from pyclusters.interface.controllers.main.transformer_controller import MainTransformerController
from pyclusters.interface.controllers.main.visualize_controller import MainVisualizerController
from pyclusters.interface.windows_manager import WindowsManager

import logging as log

from os.path import join
PYCLUSTERS_RESOURCES_RELATIVE_LOCATION = join("..", "resources")
PYCLUSTERS_MODULES_RELATIVE_LOCATION  = "modules"



class PyclusterApplication:

    def __init__(self, lib_name, lib_path ):
        self.__lib_name = lib_name
        self.__lib_path = lib_path
        self.__resources_location = join(lib_path, PYCLUSTERS_RESOURCES_RELATIVE_LOCATION)
        self.__modules_location = join(lib_path, PYCLUSTERS_MODULES_RELATIVE_LOCATION)

    def module_register(self) -> ModulesRegister:
        scanner = ModulesScanner(self.__lib_path, self.__lib_name)
        scanner.scan(PYCLUSTERS_MODULES_RELATIVE_LOCATION)
        return scanner.get_register()

    def run(self):
        # Initiating logger
        log.info("Application starting...")

        # Reading modules
        log.info( "Module location: {}".format(self.__lib_path))
        register : ModulesRegister = self.module_register()

        # Construct core
        log.info("Building core...")
        core = Core( register )

        # Building interface
        log.info("Building interface")
        windows_manager = WindowsManager(core, self.__resources_location + "/gui/gtk/main.glade")
        windows_manager.add_style(self.__resources_location + "/gui/gtk/main.css")

        windows_manager.add_window( "home_content", HomeController, is_main=True)
        windows_manager.add_window( "database_creation_content", ImportDatabaseController)
        windows_manager.add_window("project_creation_content", ProjectCreationController)
        windows_manager.add_window( "main_content", MainController)
        windows_manager.add_window( "main_content", MainVisualizerController)
        windows_manager.add_window( "main_content", MainImportController)
        windows_manager.add_window("main_content", MainAlgorithmController)
        windows_manager.add_window("main_content", MainGenericController)
        windows_manager.add_window("main_content", MainTransformerController)
        windows_manager.add_window("main_content", MainComparatorController)
        windows_manager.start()

    def resources_path(self): return self.__resources_location
    def modules_path(self): return self.__modules_location



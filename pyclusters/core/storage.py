#
# File: storage.py
# Description: Storage classes to manage stored data
# Author: Gael Marcadet
#
from pyclusters.core.entities import DatabaseEntity, Project, LabelsEntity, ConstraintsEntity
from pyclusters.core.entities import DatabaseFactory, LabelsFactory, IdentifierManager, ConstraintsFactory
from datetime import datetime

from pathlib import Path
import yaml

import os
from os.path import expanduser

import shutil
import logging as log

########################################################################################################################
# Storage configuration
########################################################################################################################
from pyclusters.exceptions.core import DatabaseNotFoundException, DatabaseAlreadyExistsException, ConflictException, \
    ConstraintsAlreadyExistsException, ConstraintsNotFoundException, ProjectNotFoundException, \
    ProjectAlreadyExistsException, NotFoundException

USER_HOME = expanduser("~")
PYCLUSTERS_STORAGE_LOCATION = USER_HOME + "/.pyclusters"

PROJECTS_DESCRIPTOR = PYCLUSTERS_STORAGE_LOCATION + "/projects.yaml"
PROJECTS_LOCATION = PYCLUSTERS_STORAGE_LOCATION + "/projects"

DATABASES_DESCRIPTOR = PYCLUSTERS_STORAGE_LOCATION + "/databases.yaml"
DATABASES_LOCATION = PYCLUSTERS_STORAGE_LOCATION + "/databases"

LABELS_LOCATION = "labels"
CONSTRAINTS_LOCATION = "constraints"
VIEWS_LOCATION = "views"



class InvalidIdentifierException(ValueError):
    def __init__(self, message):
        ValueError.__init__(self, message)

########################################################################################################################
# Storage Entities Descriptor
########################################################################################################################

class EntityDescriptor:
    def __init__(self, id, name, description, location):
        # check inputs
        assert type(id) == str, "Id must be str"
        assert type(name) == str, "Name must be str"
        assert type(location) == str, "Name must be str"
        assert type(description) == str, "Nname must be str"
        assert id != "", "id cannot be empty"

        self.id = id
        self.name = name
        self.description = description
        self.location = location

    def get_id(self): return self.id
    def get_name(self): return self.name
    def get_location(self): return self.location
    def get_description(self): return self.description if self.description else ""
    def set_name(self, name): self.name = name
    def set_description(self, description): self.description = description

class DatabaseEntityDescriptor(EntityDescriptor):
    def __init__(
            self,
            id: str,
            name: str,
            location: str,
            description: str = "",
            headers: [] = [],
            last_access : datetime = None
    ):

        EntityDescriptor.__init__(self, id, name, description, location)
        self.headers = []
        self.last_access = last_access

    def __repr__(self):
        return self.__dict__


class DatabasesCollectionDescriptor:
    @staticmethod
    def empty():
        return DatabasesCollectionDescriptor({})

    def __init__(self, databases: {str: DatabaseEntityDescriptor}):
        self.__databases: {str: DatabaseEntityDescriptor} = databases

    def find_database_descriptor_by_id(self, database_id) -> DatabaseEntityDescriptor:
        if database_id in self.__databases:
            return self.__databases[database_id]
        else:
            raise DatabaseNotFoundException(f"No database found with id {database_id}")

    def get_all_database_descriptors(self):
        return list(self.__databases.values())

    def add_database_descriptor(self, database_descriptor : DatabaseEntityDescriptor):
        if database_descriptor.id not in self.__databases:
            self.__databases[database_descriptor.id] = database_descriptor
        else:
            raise DatabaseAlreadyExistsException()

    def contains_database(self, database_id : str):
        assert type(database_id) == str, "Cannot compare database id with invalid id type"
        return database_id in self.__databases

    def __iter__(self):
        return iter(self.__databases.items())

    def __repr__(self):
        return self.__dict__

    def delete_database(self, database_descriptor : DatabaseEntityDescriptor):
        if database_descriptor.get_id() in self.__databases:
            del self.__databases[database_descriptor.get_id()]
        else:
            raise DatabaseNotFoundException()

class LabelsEntityDescriptor(EntityDescriptor):
    def __init__(self, id: str, name: str, description: str, location: str, database_id):
        # checks input
        assert type(database_id) == str, "Database id must be str"

        EntityDescriptor.__init__(self, id, name, description, location)
        self.database_id = database_id

    def get_database_id(self) -> str: return self.database_id

class ConstraintsEntityDescriptor(EntityDescriptor):
    def __init__(self, id : str, name : str, description : str, location, database_id : str):
        # checks input
        assert type(database_id) == str, "Database id must be str"
        EntityDescriptor.__init__(self, id, name, description, location)
        self.database_id = database_id

    def get_database_id(self): return self.database_id


class ProjectEntityDescriptor(EntityDescriptor):
    def __init__(
            self,
            project_id: str,
            name: str,
            project_location: str,
            database_id: str,
            labels={},
            constraints={},
            views={},
            description: str = "",
            last_access : datetime = None
    ):

        EntityDescriptor.__init__(self, project_id, name, description, project_location)

        self.constraints = constraints
        self.database_id = database_id
        self.labels: {str: LabelsEntityDescriptor} = labels
        self.views: {str: DatabaseEntityDescriptor} = views
        self.last_access = last_access

    def add_label_set(self, label_set_descriptor: LabelsEntityDescriptor):
        assert type(label_set_descriptor) == LabelsEntityDescriptor, "Clustering must be clustering descriptor"
        # checks if clustering doesn't exists yet
        if label_set_descriptor.id not in self.labels:
            self.labels[label_set_descriptor.id] = label_set_descriptor
        else:
            raise ConflictException("Label with given id already exists")

    def add_constraints(self, constraints_descriptor : ConstraintsEntityDescriptor):
        assert type(constraints_descriptor) == ConstraintsEntityDescriptor
        if constraints_descriptor.id not in self.constraints:
            self.constraints[constraints_descriptor.id] = constraints_descriptor
        else:
            raise ConstraintsAlreadyExistsException("Constraints set with given id already exists")

    def find_constraints_by_id(self, constraints_id : str) -> ConstraintsEntityDescriptor:
        if constraints_id in self.constraints:
            return self.constraints[constraints_id]
        else:
            raise ConstraintsNotFoundException()

    def find_views_by_id(self, views_id : str) -> DatabaseEntityDescriptor:
        if views_id in self.views:
            return self.views[views_id]
        else:
            raise DatabaseNotFoundException()

    def find_labels_by_id(self, labels_id: str) -> LabelsEntityDescriptor:
        if labels_id in self.labels:
            return self.labels[labels_id]
        else:
            raise NotFoundException( message="Labels with id {} not found !".format(labels_id) )

    def has_constraints(self, constraints_id : str) -> bool:
        return constraints_id in self.constraints

    def add_view(self, view_descriptor: DatabaseEntityDescriptor):
        self.views[view_descriptor.get_id()] = view_descriptor


    def get_id(self) -> str: return self.id
    def get_name(self) -> str: return self.name
    def get_description(self) -> str: return self.description
    def get_database_id(self) -> str: return self.database_id

    def get_labels(self):
        return list(self.labels.values())

    def get_views(self): return list(self.views.values())

    def get_constraints(self): return list(self.constraints.values())

    def delete_view(self, view_descriptor : DatabaseEntityDescriptor):
        if view_descriptor.get_id() in self.views:
            del self.views[view_descriptor.get_id()]

    def delete_constraints(self, constraints_descriptor : ConstraintsEntityDescriptor):
        if constraints_descriptor.get_id() in self.constraints:
            del self.constraints[constraints_descriptor.get_id()]

    def delete_labels(self, labels_descriptor : LabelsEntityDescriptor):
        if labels_descriptor.get_id() in self.labels:
            del self.labels[labels_descriptor.get_id()]


class ProjectsCollectionDescriptor:
    @staticmethod
    def empty():
        return ProjectsCollectionDescriptor({})

    def __init__(self, projects: {str : ProjectEntityDescriptor}):
        self.__projects : {str: ProjectEntityDescriptor} = projects

    def __repr__(self):
        return self.__dict__

    def contains_project_with_id(self, project_id: str) -> bool:
        '''
        Returns True if a project is bind with given project id, False otherwise
        :param project_id: Project's identfiier
        :return: True if pro
        '''
        return project_id in self.__projects

    def find_project_descriptor_by_id(self, project_id: str) -> ProjectEntityDescriptor:
        """
        Returns a project bind with given project id.

        :param project_id: Project's identifier
        :return: Project's descriptor.

        :raise ProjectNotFoundException: When provided id not match with any project
        """
        if project_id in self.__projects:
            return self.__projects[project_id]
        else:
            raise ProjectNotFoundException("Project descriptor not found with id {}".format(project_id))

    def get_all_projects_descriptor(self) -> [ProjectEntityDescriptor]:
        """
        Returns all project descriptors.
        :return: All project descriptors.
        """
        return list(self.__projects.values())

    def add_project_descriptor(self, project_descriptor : ProjectEntityDescriptor):
        if project_descriptor.id not in self.__projects:
            self.__projects[ project_descriptor.id] = project_descriptor
        else:
            raise ProjectAlreadyExistsException()

    def delete_project(self, project_descriptor : ProjectEntityDescriptor):
        if project_descriptor.id in self.__projects:
           del  self.__projects[project_descriptor.id]
        else:
            raise ProjectNotFoundException()

########################################################################################################################
# Storage Input/Output Objects
########################################################################################################################

class DatabaseIO:
    HEADERS_LINE_PREFIX = "h:"
    HEADER_SEPARATOR = "###"
    def read(self, database_descriptor: DatabaseEntityDescriptor) -> DatabaseEntity:
        assert type(database_descriptor) == DatabaseEntityDescriptor, "Cannot read database due to invalid descriptor type"

        headers = []
        data = []
        with open(database_descriptor.location, 'r') as f:
            for line in f:
                # read line as headers when start with specific prefix
                if line.startswith(DatabaseIO.HEADERS_LINE_PREFIX):
                    textual_headers_line = line.replace(DatabaseIO.HEADERS_LINE_PREFIX, '')
                    textual_headers_line = textual_headers_line.replace("\n", "")
                    if textual_headers_line != '':
                        headers = textual_headers_line.split(DatabaseIO.HEADER_SEPARATOR)
                    continue

                # read each instance in one line
                line = line.strip()
                if line != '':
                    data_string = line.split()
                    d = [float(i.strip()) for i in data_string if i != '']
                    data.append(d)

        return DatabaseFactory.build(
            id=database_descriptor.id,
            name=database_descriptor.name,
            description=database_descriptor.description,
            data=data,
            headers=headers
        )

    def write(self, filename : str, database: DatabaseEntity):
        """
        Writes given database at specified location represented by filename.

        :param filename: Location where database will be written.
        :param database: Written database.
        """
        assert type(filename) == str, "Filename must be str"
        with open(filename, 'w') as f:
            # writes headers in one line only if are present
            if database.has_headers():
                headers_line = DatabaseIO.HEADERS_LINE_PREFIX + DatabaseIO.HEADER_SEPARATOR.join(database.get_headers())
                f.write( headers_line + "\n" )

            # writes data, one instance by line
            for instance in database.get_data():
                string_instance = [str(v) for v in instance]
                string_instance.append("\n")
                f.write(" ".join(string_instance))


class LabelSetIO:
    def read(self, label_set_descriptor: LabelsEntityDescriptor) -> LabelsEntity:
        assert type(label_set_descriptor) == LabelsEntityDescriptor, "Cannot read clustering due to invalid descriptor type"

        labels = []
        with open(label_set_descriptor.location, 'r') as f:
            for line in f:
                if line != '':
                    labels.append(float(line))

        return LabelsFactory.build(
            id = label_set_descriptor.id,
            name=label_set_descriptor.name,
            description=label_set_descriptor.description,
            database_id=label_set_descriptor.get_database_id(),
            labels=labels
        )

    def write(self, filename: str, label_set: LabelsEntity):
        assert type(filename) == str, "Filename must be str"
        assert type(label_set) == LabelsEntity, "Clustering must be a Clustering object"

        with open(filename, 'w') as f:
            for label in label_set.get_data():
                f.write(" ".join([str(label), "\n"]))




class ConstraintsIO:
    MUST_LINK_CONSTRAINT=1
    CANNOT_LINK_CONSTRAINT=-1
    def read(self, constraints_descriptor : ConstraintsEntityDescriptor) -> ConstraintsEntity:
        ml = []
        cl = []

        with open( constraints_descriptor.get_location(), 'r' ) as f:
            for line in f:
                constraint = [ int(n) for n in line.split() ]
                x, y, kind = constraint
                if kind == ConstraintsIO.MUST_LINK_CONSTRAINT:
                    ml.append( [x, y] )
                elif kind == ConstraintsIO.CANNOT_LINK_CONSTRAINT:
                    cl.append( [x, y] )
                else:
                    raise ValueError("Invalid constraints type")


        return ConstraintsFactory.build(
            id = constraints_descriptor.id,
            name = constraints_descriptor.name,
            description=constraints_descriptor.description,
            database_id=constraints_descriptor.get_database_id(),
            ml = ml,
            cl = cl
        )

    def write(self, filename : str, constraints_set : ConstraintsEntity):
        with open(filename, 'w') as f:
            # writes must-links constraints
            for x, y in constraints_set.get_ml():
                f.write(f"{x} {y} {ConstraintsIO.MUST_LINK_CONSTRAINT}\n")

            # writes cannot-links constraints
            for x, y in constraints_set.get_cl():
                f.write(f"{x} {y} {ConstraintsIO.CANNOT_LINK_CONSTRAINT}\n")

########################################################################################################################
# Storage Utility Object
########################################################################################################################


"""
Storage Facade can be used to access easily stored projects.
"""
class StorageFacade:

    def __init__(self):
        # retreives i/o objects in order to manager database, constraints and labels set.
        self.databaseio = DatabaseIO()
        self.labelsio = LabelSetIO()
        self.constraintsio = ConstraintsIO()

        # checks that pyclusters home is valid
        self.__init_home_if_not_exists()

        # read project and database managers
        self.projects_set_descriptor = self.__read_projects_set_descriptor()
        self.database_set_descriptor = self.__read_databases_set_descriptor()

        assert self.projects_set_descriptor is not None, "Projets set descriptor is None"

    def find_all_projects(self) -> [ProjectEntityDescriptor]:
        return self.projects_set_descriptor.get_all_projects_descriptor()

    def find_project_by_id(self, project_id: str) -> Project:
        project_desriptor = self.projects_set_descriptor.find_project_descriptor_by_id( project_id )
        return self.__bootstrap_project( project_desriptor )

    def find_all_database(self) -> [DatabaseEntityDescriptor]:
        return self.database_set_descriptor.get_all_database_descriptors()

    def find_database_by_id(self, database_id: str) -> DatabaseEntity:
        database_descriptor =  self.database_set_descriptor.find_database_descriptor_by_id( database_id )
        return self.__bootstrap_database(database_descriptor)

    def find_constraints_by_id(self, project_id : str, constraints_id) -> ConstraintsEntity:
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project_id)
        if project_descriptor.has_constraints(constraints_id):
            constraints_descriptor = project_descriptor.find_constraints_by_id( constraints_id )
            return self.__bootstrap_constraints_set( constraints_descriptor )
        else:
            raise ConstraintsNotFoundException()



    def import_database(self, database: DatabaseEntity):
        # checks input
        assert type(database) == DatabaseEntity, "Added database must be a database entity"

        # checks if database is unique
        try:
            self.find_database_by_id(database.get_name())
            raise DatabaseNotFoundException()
        except NotFoundException:
            pass

        # create database descriptor
        database_location = os.path.join( DATABASES_LOCATION, database.get_id() )
        database_descriptor = DatabaseEntityDescriptor(
            id = database.get_id(),
            name = database.get_name(),
            description=database.get_description(),
            location = database_location,
            headers = database.get_headers()
        )

        # duplicate database locally
        self.databaseio.write(database_descriptor.location, database)

        # appends database and save descriptor
        self.database_set_descriptor.add_database_descriptor(database_descriptor)
        self.__save()

    def add_constraints(self, project : Project, constraints: ConstraintsEntity):
        assert type(project) == Project, "Project's identifier must be str"
        assert type(constraints) == ConstraintsEntity, "Constraints must be a Constraints object"

        # a clustering cannot be added to an non-existing project
        project_id = project.get_id()
        if not self.projects_set_descriptor.contains_project_with_id(project_id):
            raise ProjectNotFoundException( f"No project found with id {project_id}")

        # checks that constraints identifier is valid
        if not IdentifierManager.is_valid_identifier( constraints.get_id() ):
            raise ValueError("Invalid constraints identifier")

        # checks that required params are non empty
        if constraints.get_name() == "":
            raise ValueError("Constraints name cannot be empty")

        # loads project descriptor and save clustering locally
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id( project_id )
        constraints_location = os.path.join( project_descriptor.location, CONSTRAINTS_LOCATION, constraints.get_id() )
        self.constraintsio.write( constraints_location, constraints )

        # creates constraints descriptor and save it under project descriptor
        constraints_descriptor = ConstraintsEntityDescriptor(
            id = constraints.get_id(),
            name = constraints.get_name(),
            description = constraints.get_description(),
            location = constraints_location,
            database_id=constraints.get_database_id()
        )
        project_descriptor.add_constraints( constraints_descriptor )
        self.__save()

    def add_project(self, project : Project):
        return self.create_project(
            project_id = project.get_id() ,
            project_name = project.get_name(),
            project_description = project.get_description(),
            database_id=project.get_database().get_id()
        )

    def create_project(self, project_id : str, project_name: str, project_description: str, database_id: str) -> Project:
        # checks inputs type
        assert type(project_id) == str, "Project's identifier must be str"
        assert type(project_name) == str, "Project's name must be str"
        assert type(project_description) == str, "Project's description must be str"
        assert type(database_id) == str, "Linked database name must be str"

        # ensures that project identifier is valid
        if not IdentifierManager.is_valid_identifier( project_id ):
            raise InvalidIdentifierException( f"Invalid project identifier: {project_id}")

        # check if project is unique
        # fail on conflict if database not found
        if self.projects_set_descriptor.contains_project_with_id(project_id):
            raise ProjectAlreadyExistsException(f"Project with id {project_id} already exists")

        # check if database exists
        # fail on not found if database not exists
        if not self.database_set_descriptor.contains_database( database_id ):
            raise DatabaseNotFoundException(f"Database with identifier {database_id} not found")

        # init project's labels and constraints directories
        project_location = os.path.join(PROJECTS_LOCATION, project_id)

        label_directory = os.path.join( project_location, LABELS_LOCATION )
        path = Path(label_directory)
        path.mkdir(parents=True)

        constraints_directory = os.path.join(project_location, CONSTRAINTS_LOCATION)
        path = Path(constraints_directory)
        path.mkdir(parents=True)

        views_directory = os.path.join(project_location, VIEWS_LOCATION)
        path = Path(views_directory)
        path.mkdir(parents=True)

        # inserting projects location and save project on disk
        project_descriptor = ProjectEntityDescriptor(
            project_id = project_id,
            name=project_name,
            description=project_description,
            project_location=project_location,
            database_id=database_id,
            last_access=datetime.now()
        )

        self.projects_set_descriptor.add_project_descriptor( project_descriptor )
        self.__save()

        return self.__bootstrap_project( project_descriptor )

    def add_view(self, project: Project, view: DatabaseEntity):
        # retrieve project descriptor from project
        if not self.projects_set_descriptor.contains_project_with_id(project.id):
            raise ProjectNotFoundException()
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.id)

        # create views directory under project
        # create view directory if not exists
        view_filename = view.get_id()
        view_location = os.path.join(project_descriptor.location, VIEWS_LOCATION, view_filename)
        view_directory = os.path.dirname(view_location)
        if not os.path.exists(view_directory):
            os.mkdir(view_directory)


        # creates clustering descriptor
        view_descriptor = DatabaseEntityDescriptor(
            id=view.get_id(),
            name=view.get_name(),
            description=view.get_description(),
            location=view_location
        )

        # save labels into location
        self.databaseio.write(view_location, view)

        # update
        project_descriptor.add_view(view_descriptor)
        self.__save()

    def add_labels_set(self, project: Project, labels_set : LabelsEntity):
        # checks input manager
        assert type(project) == Project, "Cannot insert clustering in non Project object"
        assert type(labels_set) == LabelsEntity, "Only labels are allowed"
        # checks clustering type
        assert type(labels_set.get_name()) == str, "Invalid clustering name type"
        assert type(labels_set.get_description()) == str, "Invalid clustering description type"

        # retreive project descriptor
        if not self.projects_set_descriptor.contains_project_with_id( project.id ):
            raise ProjectNotFoundException()
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id( project.id )

        # create clustering location
        clustering_filename = labels_set.get_id()
        clustering_location =  os.path.join(project_descriptor.location, LABELS_LOCATION, clustering_filename)

        # creates clustering descriptor
        clustering_descriptor = LabelsEntityDescriptor(
            id = labels_set.get_id(),
            name=labels_set.get_name(),
            description=labels_set.get_description(),
            database_id=labels_set.get_database_id(),
            location=clustering_location
        )

        # save labels into location
        self.labelsio.write(clustering_descriptor.location, labels_set)

        # update
        project_descriptor.add_label_set(clustering_descriptor)
        self.__save()


    def project_identifier_exists(self, project_id : str):
        return self.projects_set_descriptor.contains_project_with_id(project_id)

    def update_project_last_access(self, project_id : str):
        project_descriptor : ProjectEntityDescriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project_id)
        project_descriptor.last_access = datetime.now()
        self.__save()

    def update_database_last_access(self, database_id : str):
        database_descritor : DatabaseEntityDescriptor = self.database_set_descriptor.find_database_descriptor_by_id(
            database_id
        )
        database_descritor.last_access = datetime.now()
        self.__save()



    def __init_home_if_not_exists(self):
        # checks pyclusters home
        if not os.path.exists(PYCLUSTERS_STORAGE_LOCATION):
            os.mkdir(PYCLUSTERS_STORAGE_LOCATION)

        # checks projects descriptor and location
        if not os.path.exists(PROJECTS_DESCRIPTOR):
            projects = ProjectsCollectionDescriptor.empty()
            self.__write_projects_set_descriptor(projects)

        if not os.path.exists(PROJECTS_LOCATION):
            os.mkdir(PROJECTS_LOCATION)

        # checks database descriptor and location
        if not os.path.exists(DATABASES_DESCRIPTOR):
            databases = DatabasesCollectionDescriptor.empty()
            self.__write_databases_set_descriptor(databases)

        if not os.path.exists(DATABASES_LOCATION):
            os.makedirs(DATABASES_LOCATION)

    def __get_all_projects_descriptor(self) -> [ProjectEntityDescriptor]:
        return self.projects_set_descriptor.get_all_projects_descriptor()

    def __write_projects_set_descriptor(self, projects: ProjectsCollectionDescriptor):
        with open(PROJECTS_DESCRIPTOR, 'w') as f:
            yaml.dump(projects, f)

    def __write_databases_set_descriptor(self, databases: DatabasesCollectionDescriptor):
        with open(DATABASES_DESCRIPTOR, 'w') as f:
            yaml.dump(databases, f)

    def __read_projects_set_descriptor(self) -> ProjectsCollectionDescriptor:
        with open(PROJECTS_DESCRIPTOR, 'r') as f:
            return yaml.load(f, Loader=yaml.FullLoader)

    def __read_databases_set_descriptor(self) -> DatabasesCollectionDescriptor:
        with open(DATABASES_DESCRIPTOR, 'r') as f:
            return yaml.load(f, Loader=yaml.FullLoader)

    def __get_all_database_descriptor(self):
        return self.database_set_descriptor.get_all_database_descriptors()

    def __bootstrap_constraints_set(self, constraints_descriptor : ConstraintsEntityDescriptor) -> ConstraintsEntity:
        assert type(constraints_descriptor) == ConstraintsEntityDescriptor, "Invalid constraints descriptor type"
        return self.constraintsio.read(constraints_descriptor)

    def __bootstrap_label_set(self, labels_set_descriptor: LabelsEntityDescriptor) -> LabelsEntity:
        assert type(labels_set_descriptor) == LabelsEntityDescriptor, "Invalid clustering descriptor type"
        try:
            return self.labelsio.read(labels_set_descriptor)
        except FileNotFoundError as e:
            log.exception(e)
            raise NotFoundException()

    def __bootstrap_database(self, database_descriptor: DatabaseEntityDescriptor) -> DatabaseEntity:
        return self.databaseio.read(database_descriptor)

    def __bootstrap_project(self, project_descriptor: ProjectEntityDescriptor) -> Project:
        assert type(project_descriptor) == ProjectEntityDescriptor, "Cannot bootstrap project with invalid input type"

        # load data
        database: DatabaseEntity = self.find_database_by_id(project_descriptor.database_id)

        # load all clustering
        labels_list = []
        for labels_set_descriptor in project_descriptor.get_labels():
            label_set = self.__bootstrap_label_set(labels_set_descriptor)
            labels_list.append(label_set)

        # loads all constraints
        constraints_set_list = []
        for constraints_descriptor in project_descriptor.get_constraints():
            constraints = self.__bootstrap_constraints_set(constraints_descriptor)
            constraints_set_list.append(constraints)

        # loads all views
        views = []
        for view_descriptor in project_descriptor.get_views():
            view = self.__bootstrap_database( view_descriptor )
            views.append(view)

        return Project(
            id = project_descriptor.id,
            name=project_descriptor.name,
            description=project_descriptor.description,
            database=database,
            labels = labels_list,
            constraints=constraints_set_list,
            views=views
        )


    def __save(self):
        self.__write_projects_set_descriptor(self.projects_set_descriptor)
        self.__write_databases_set_descriptor(self.database_set_descriptor)

    def update_view(self, project : Project, view : DatabaseEntity):
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.get_id())
        view_id = view.get_id()
        view_descriptor = project_descriptor.find_views_by_id(view_id)
        view_descriptor.set_name( view.get_name() )
        self.__save()

    def update_labels(self, project : Project, labels : LabelsEntity ):
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.get_id())
        labels_descriptor = project_descriptor.find_labels_by_id(labels.get_id())
        labels_descriptor.set_name(labels.get_name())
        self.__save()

    def update_constraints(self, project : Project, constaints : ConstraintsEntity ):
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.get_id())
        constraints_descriptor = project_descriptor.find_constraints_by_id(constaints.get_id())
        constraints_descriptor.set_name(constaints.get_name())
        self.__save()

    def delete_view(self, project : Project, view : DatabaseEntity):
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.get_id())
        view_descriptor = project_descriptor.find_views_by_id(view.get_id())
        project_descriptor.delete_view( view_descriptor )
        os.remove(view_descriptor.get_location())
        self.__save()

    def delete_constraints(self, project : Project, constraints : ConstraintsEntity):
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.get_id())
        constraints_descriptor = project_descriptor.find_constraints_by_id(constraints.get_id())
        project_descriptor.delete_constraints( constraints_descriptor )
        os.remove(constraints_descriptor.get_location())
        self.__save()

    def delete_labels(self, project : Project, labels : LabelsEntity):
        project_descriptor = self.projects_set_descriptor.find_project_descriptor_by_id(project.get_id())
        labels_descriptor = project_descriptor.find_labels_by_id( labels.get_id() )
        project_descriptor.delete_labels( labels_descriptor )
        os.remove( labels_descriptor.get_location() )
        self.__save()

    def delete_project(self, project_id : str):
        project_descriptor : ProjectEntityDescriptor = self.projects_set_descriptor.find_project_descriptor_by_id(
            project_id
        )

        # remove project from project container
        self.projects_set_descriptor.delete_project( project_descriptor )
        self.__save()

        # remove project from disk
        # as project can contains project related data, project is a folder !
        log.debug("Deleting project at location {}".format(project_descriptor.get_location()))
        shutil.rmtree(project_descriptor.get_location())

    def delete_database(self, database_id : str ):
        # ensures that no project requires removed database
        for projetc_descriptor in self.find_all_projects():
            if projetc_descriptor.get_database_id() == database_id:
                raise ValueError("Cannot delete a database which is linked with project")

        # delete database from databases container
        database_descriptor = self.database_set_descriptor.find_database_descriptor_by_id(database_id)
        self.database_set_descriptor.delete_database( database_descriptor )
        self.__save()

        # delete database from disk
        # remember that database is a file !
        log.debug("Deleting database at location {}".format(database_descriptor.get_location()))
        os.remove(database_descriptor.get_location())


if __name__ == '__main__':
    storage = StorageFacade()
    if False:
        database = DatabaseFactory.build(
            id = "ma_database",
            name="Mon super BDD",
            description="Ceci est un très petite bd",
            data = [[1, 2], [2, 3], [3, 4], [4, 5]]
        )
        print(database.__dict__)

        storage.import_database( database )
        storage.create_project("premierprojet", "Mon pre", "descr", "ma_database")
        project = storage.find_project_by_id("premierprojet")
        print( storage.find_all_projects()[0].last_access)

        labels = LabelSetFactory.build(
            id="premierlabels",
            name="Mon premier label",
            labels=[1, 2, 3, 4]
        )
        storage.add_labels_set( project, labels )

    if False:
        constraints = ConstraintsSetFactory.build(
            id = "constraintsid",
            name="constraints_name",
            description = "constraints_description",
            cl=[[1,2]],
            ml=[[2,3]]
        )
        storage.add_constraints( "premierprojet", constraints  )
    project = storage.find_project_by_id("premierprojet")


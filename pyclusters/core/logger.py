LOGGER_NO_LOG = 5
LOGGER_ERROR_LEVEL = 4
LOGGER_WARN_LEVEL = 3
LOGGER_INFO_LEVEL = 2
LOGGER_DEBUG_LEVEL = 1

LOGGER_LEVELS = [
    LOGGER_NO_LOG,
    LOGGER_ERROR_LEVEL,
    LOGGER_WARN_LEVEL,
    LOGGER_INFO_LEVEL,
    LOGGER_DEBUG_LEVEL
]


class Logger:
    def debug(self, message): pass

    def info(self, message): pass

    def warning(self, message): pass

    def error(self, message): pass


class StandardOutputLogger(Logger):
    def __init__(self, level):
        self.level = level

    def debug(self, message):
        if self.level <= LOGGER_DEBUG_LEVEL:
            print("[DEBUG] {}".format(message))

    def info(self, message):
        if self.level <= LOGGER_INFO_LEVEL:
            print("[Info] {}".format(message))

    def warning(self, message):
        if self.level <= LOGGER_WARN_LEVEL:
            print("[Warning] {}".format(message))

    def error(self, message):
        if self.level <= LOGGER_ERROR_LEVEL:
            print("[Error] {}".format(message))


class NoLogger(Logger): pass


class LoggerFactory:

    @staticmethod
    def build_logger(level=LOGGER_INFO_LEVEL):
        return NoLogger() if level == LOGGER_NO_LOG else StandardOutputLogger(level)


class ActionAbortError(Exception):
    def __init__(self, message = None):
        Exception.__init__(self, message)

class InvalidConfiguationException( Exception ):
    def __init__(self, message = None):
        Exception.__init__(self, message)

class ModuleExecutionError(Exception):
    def __init__(self, message = None, title = "Module Execution Error"):
        Exception.__init__(self, message)
        self.__message = message
        self.__title = title

    def get_message(self): return self.__message

    def get_title(self): return self.__title

class InvalidModuleReturnError(ModuleExecutionError):
    def __init__(self, message):
        ModuleExecutionError.__init__( self, title="Invalid returned value", message=message )
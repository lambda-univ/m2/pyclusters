class InvalidFileFormatError(IOError):
    """Invalid file format error.
    """
    def __init__(self, message = None):
        IOError.__init__(self, message)
        self.__message = message

    def message(self):
        return self.message

class InvalidConstraintsError(Exception):
    """Invalid constraints error.
    Raised when invalid constraints.
    """
    def __init__(self, message = None):
        Exception.__init__(self, message)

class InvalidLabelsError(Exception):
    def __init__(self, message = None):
        Exception.__init__(self, message)

class NotFoundException(Exception):
    def __init__(self, message = "Entity not found"):
        Exception.__init__(self, message)


class DatabaseNotFoundException(NotFoundException):
    def __init__(self, message = "Dazbase not found"):
        NotFoundException.__init__(self, message)

class ViewNotFoundException(NotFoundException):
    def __init__(self, message = "View not found"):
        NotFoundException.__init__(self, message)

class ProjectNotFoundException(NotFoundException):
    def __init__(self, message = None):
        NotFoundException.__init__(self, message)

class LabelsNotFoundException(NotFoundException):
    def __init__(self, message = None):
        NotFoundException.__init__(self, message)


class ClusteringNotFoundException(NotFoundException):
    def __init__(self, message = None):
        NotFoundException.__init__(self, message)


class ConflictException(Exception): pass


class ProjectAlreadyExistsException(ConflictException): pass


class DatabaseAlreadyExistsException(ConflictException): pass


class ClusteringAlreadyExistsException(ConflictException): pass

class ConstraintsAlreadyExistsException(ConflictException): pass

class ConstraintsNotFoundException(NotFoundException): pass

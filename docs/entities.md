# Entities
To represent database, labels and constraints, PyClusters uses 
different objects. All of them must be represented with a unique identifier, a name and other
attributes dependant of their type.

## Database
As PyClusters need datasets to work, these are represented with only one object: **Database**.
A `Database` object is defined with a `data` Numpy two-dimensional array and an optional `headers` 
one-dimensional string array.

### Database Creation
To create a database instance in module for example in a `DatabaseIOModule` submodule, prefer using static method
`DatabaseFactory.build_from_data( data : [[Union[ float, np.ndarray ]]], headers : [str] )` instead of creating `Database` object itself.
`data` must be a two-dimensional array of float and headers a list of string.

```python
from pyclusters.core.entities import Database, DatabaseFactory
from pyclusters.core.interface import DatabaseIOModule


class MyDatabaseIOModule(DatabaseIOModule):
    """My First Database IO module"""
    def write(self, filename: str, database: Database): ...

    def read(self, filename: str) -> Database:
        data, headers = ...  # read from filename
        return DatabaseFactory.build_from_data(
            data = data,
            headers=headers
        )
```


### Database Methods

| Method | Description |
| --- |--- |
| **get_data() -> np.ndarray** | Returns Numpy data array | 
| **shape()** | Returns Data shape in form *(N, K)* where N is instances number and K the instance attributes | 
| **get_headers() -> np.ndarray** | Returns one-dimensional string headers array | 
| **has_headers() -> bool** | As headers is optional, returns True if database has headers, False otherwise. | 


## Constraints
`Constraints` object represents both **Must-Link** and **Cannot-Link** constraints.

### Constraints Creation
As the same as `Database`, this object must be created with 
`ConstraintsFactory.build_from_data(ml : [(int, int)], cl : [(int, int)])`.
`ml` and `cl` must be a two-dimensional array of integer, an array of couple of integer or a Numpy equivalent.

A constraint (whereas Must-Link or Cannot-Link) is defined by two integers x and y corresponding to instances 
at position x and y in a database.

Let's see an example to create a new `Constraints` instance:

```python
from pyclusters.core.entities import ConstraintsFactory, Constraints
from pyclusters.core.interface import ConstraintsIOModule
from pyclusters.exceptions.core import InvalidFileFormatError

class MyConstraintsIOModule(ConstraintsIOModule):
    MUST_LINK_CONSTRAINT = 1
    CANNOT_LINK_CONSTRAINT = -1

    def write(self, filename: str, constraints: Constraints):
        ...

    def read(self, filename: str) -> Constraints:
        ml = []
        cl = []

        with open(filename, 'r') as f:
            for line in f:
                # read constraint line and checks that there is three int inside
                constraint = [int(n) for n in line.split()]
                if len(constraint) != 3:
                    raise InvalidFileFormatError("Constraints line invalid")
                
                # read constraint
                x, y, kind = constraint
                if kind == MyConstraintsIOModule.MUST_LINK_CONSTRAINT:
                    ml.append([x, y])
                elif kind == MyConstraintsIOModule.CANNOT_LINK_CONSTRAINT:
                    cl.append([x, y])
                else:
                    raise InvalidFileFormatError("Invalid constraints type")

        return ConstraintsFactory.build_from_data(
            ml=ml,
            cl=cl
        )
```


### Constraints Methods

| Methods | Description |
| --- | -- |
| get_ml() | Returns Must-Link constraints |
| get_cl() | Returns Cannot-Link constraints |



## Labels
`Labels` object represents an affection of all database's instances.

### Labels Creation
To create a new `Labels` instance, you must use `LabelsFactory.build_from_data(labels : [int])`.
`labels` must be a one-dimensional array of integer or Numpy.

```python 
from pyclusters.core.entities import Labels
from pyclusters.core.entities import LabelsFactory
from pyclusters.core.interface import LabelsIOModule


class MyLabelsIOModule(LabelsIOModule):
    def write(self, filename: str, label_set: Labels):
        pass

    def read(self, filename: str) -> Labels:
        labels = []
        with open(filename, 'r') as f:
            for line in f:
                if line != '':
                    labels.append(float(line))
        return LabelsFactory.build_from_data(
            labels=labels
        )
```

### Labels Methods
| Method | Description |
| -- | -- |
| get_labels() -> np.ndarray | Returns numpy one-dimensional array. |
| nb_instances() -> int | Returns labels number |

# Installation
To install PyClusters, you must install some dependencies.
```shell
# install GTK
sudo apt-get install libgtk-3-dev

# download project
git clone url-to-repository.git

# install python3 PyCluters dependencies.
pip3 install -r requirements.txt

# install PyClusters
python3 setup.py install
```

You're free to install PyClusters with `pip3 install -e .`.
This command will install PyClusters but keep all files locally.
# Constrained Clustering Theory
As this project was created for research, it's important to understand 
fundamentals: cosntrained clustering.

## Clustering
[Clustering](https://en.wikipedia.org/wiki/Cluster_analysis) belongs at unsupervised machine learning technics 
where computer try to group instances of a dataset by their attributes.

Let's take an example below:

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Cluster-2.svg/440px-Cluster-2.svg.png)

In this, there are three clusters represented here with green, red and blue colors.


## Constrained clustering
The [Constrained Clustering](https://en.wikipedia.org/wiki/Constrained_clustering) is about the same as Clustering.
A difference exists with relations between instances, defined as a feedback by a user.
For instance, let's take previous example and imagine that a domain expert estimate that produced clustering
isn't prefect (a bad classified movie, instance on doubt frontier from multiple clusters, ...).
Our expert may want to modify clustering by adding constraints between instances.
In literature, multiple constraints exists but PyClusters considered only two fundamentals one:

- **Must-Link constraint**: Denotes as ML(x, y) where x and y are instances of dataset, explains that these two points 
must be classified in the same cluster.
- **Cannot-Link constraint**: Denotes as CL(x, y) where x and y are instances of dataset, explains that these two points
must be classified in different clusters.
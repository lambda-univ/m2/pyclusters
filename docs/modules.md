# PyClusters and Module
PyClusters support modules integration in order to increase features.

## Modules

![](resources/pyclusters%20-%20modules.jpg)

### Input/Output Modules
Input/Output modules allows to support new database, labels or constraints file formats.


#### DatabaseIOModule
`DatabaseIOModule` allows reading and writing database.
To do this, you must create a new class which inherits from `DatabaseIOModule`
and overwrite `write( filename : str, database : Database )` to write a database on a file
and `read(filename: str) -> Database` to read a database from a file.

```python
from pyclusters.core.entities import Database, DatabaseFactory
from pyclusters.core.interface import DatabaseIOModule


class TextualInstanceByLineDatabaseIOModule(DatabaseIOModule):
    """Reads a database in a textual format with one instance by line.
    The first line is always ignored because it's the instances number. 
    """

    def write(self, filename: str, database: Database):
        with open(filename, "w") as f:
            for instance in database.get_data():
                line = " ".join([str(x) for x in instance])
                f.write(line + "\n")

    def read(self, filename: str) -> Database:
        data = []
        with open(filename, 'r') as f:
            lines = f.readlines()[1:]
            # if line is redces
            for line in lines:
                # read each instance in one line
                line = line.strip()
                if line != '':
                    data_string = line.split()
                    d = [float(i.strip()) for i in data_string if i != '']
                    data.append(d)
        return DatabaseFactory.build_from_data(data)
```

#### LabelsIOModule
`LabelsIOModule` allows reading and writing labels.
To do this, you must create a new class which inherits from `LabelsIOModule`
and overwrite `write( filename : str, labels : Labels )` to write a database on a file
and `read(filename: str) -> Labels` to read a database from a file.

```python
from pyclusters.core.entities import Labels, LabelsFactory
from pyclusters.core.interface import LabelsIOModule


class TextualLabelByLineLabelsIOModule(LabelsIOModule):
    """Reads and writes label from textual format with one label by line."""

    def write(self, filename: str, labels: Labels):
        with open(filename, 'r') as f:
            for label in labels:
                f.write("{}\n".format(label))

    def read(self, filename: str):
        labels = []
        with open(filename, 'r') as f:
            for line in f:
                line = line.strip().replace("\n", "")
                if line != '':
                    labels.append(float(line))
        return LabelsFactory.build_from_data(labels=labels)
```

#### ConstraintsIOModule
`ConstraintsIOModule` allows reading and writing contraints.
To do this, you must create a new class which inherits from `ConstraintsIOModule`
and overwrite `write( filename : str, constraints : Constraints )` to write a database on a file
and `read(filename: str) -> Constraints` to read a database from a file.

### Visualizer
`Visualizer` module allows `Database` and `Database`/`Labels`(also called clustering) plotting.
There is a semantic difference between these two kind of visualizer.
The first one plots only database whereas the second one can plot database and labels.

Note that if you want to create a visualizer module which doesn't matter about `Labels`, you're free 
to create a new module which inherits from both `DatabaseVisualizerModule` and 
`ClusteringVisualizerModule`, thanks to python multiple inheritance system.

#### DatabaseVisualizerModule
`DatabaseVisualizerModule` allows you to plot `Database` only. 
To do that, create a new class which inherits from it and overwrite method
`visualize_database(self, database: Database, parameters) -> Figure`.
`Figure`  is a matplotlib canvas where you can draw anything you want.

```python
from matplotlib.figure import Figure
from pyclusters.core.entities import  Database
from pyclusters.core.interface import DatabaseVisualizerModule


class LinearVisualizerModule(DatabaseVisualizerModule):
    """
    Database Visualizer Module which plots only first column in a Linear way.
    """
    def visualize_database(self, database: Database, parameters) -> Figure:
        f = Figure()
        a = f.add_subplot(111)
        data = database.get_data().transpose()
        a.scatter(data[0], data[0])

        return f
```

In addition, if parameters are required, you could overwrite `require() -> ParametersBag` method. For more details, check 
[Parameters page](/modules/#parameters).

#### ClusteringVisualizerModule

`ClusteringVisualizerModule` allows you to plot `Database` and `Labels`. 
To do that, create a new class which inherits from it and overwrite method
`visualize_clustering(self, database: Database, labels : Labels, parameters) -> Figure`.
`Figure`  is a matplotlib canvas where you can draw anything you want.

```python
from matplotlib.figure import Figure
from pyclusters.core.entities import Database, Labels
from pyclusters.core.interface import DatabaseVisualizerModule, ClusteringVisualizerModule


class LinearVisualizerModule(DatabaseVisualizerModule, ClusteringVisualizerModule):
    """
    Database Visualizer Module which plots only first column in a Linear way.
    """

    def visualize_database(self, database: Database, parameters) -> Figure: ...  # Previous code


    def visualize_clustering(self, database: Database, labels: Labels, parameters) -> Figure:
        return self.visualize_database( database, parameters )
```

In addition, if parameters are required, you could overwrite `require() -> ParametersBag` method. For more details, check 
[Parameters page](/modules/#parameters).

### ClusteringAlgorithmModule
`ClusteringAlgorithm` allows you to execute a clustering algorithm (e.g. [COP-KMeans](https://github.com/Behrouz-Babaki/COP-Kmeans))
and found a `Labels` from a `Database` and `Constraints`.

To do that, create a new class which inherits from it and overwrite method 
`predict(self, database: Database, constraints: Constraints, parameters) -> Labels`.

```python
from pyclusters.core.entities import DatabaseEntity, ConstraintsEntity, Labels, LabelsFactory
from pyclusters.core.interface import ClusteringAlgorithmModule


class OneClassClusteringAlgorithmsModule(ClusteringAlgorithmModule):
    """
    Affect all instances in the same class.
    """
    def predict(self, database: DatabaseEntity, parameters, constraints: ConstraintsEntity) -> Labels:
        labels = [0 for _ in range(database.nb_instances())]
        return LabelsFactory.build_from_data(labels)
```

In addition, if parameters are required, you could overwrite `require() -> ParametersBag` method. For more details, check 
[Parameters page](/modules/#parameters).

### DatabaseTransformerModule
`DatabaseTransformerModule` allows you to transform a `Database` object to another, useful for example when you want to apply a 
normalization. To do that, create a class which inherits from `DatabaseTranformerModule` and 
overwrite `transform(self, database : Database, parameters) -> Database` method.

In addition, if parameters are required, you could overwrite `require() -> ParametersBag` method. For more details, check 
[Parameters page](/modules/#parameters).

### ComparatorModule
`ComparatorModule` allows you to compare two labels sets.
It's a good tool to compare for instance precision between true labels and predictions labels.

To create a new comparator module, create a class which inherits from `ComparatorModule` and 
overwrite `compare(self, printer: OutputStream, first_labels: Labels, second_labels: Labels, parameters)`
function. No return is needed.

In addition, if parameters are required, you could overwrite `require() -> ParametersBag` method. For more details, check 
[Parameters page](/modules/#parameters).

### GenericModule
`GenericModule` is quite different from other modules.
His main goal is to display information on a database, labels, constraints or a combination of them, on a dedicated
application section. This module is a really swissknife to get some information about what you want.
No return is needed.

To create a new `GenericModule`, create a new class which inherits from it and 
overwrite `run` method like this:

```python
from typing import Optional

from pyclusters.core.entities import Database, Constraints, Labels
from pyclusters.core.interface import GenericModule, OutputStream


class MyGenericModule(GenericModule):
    def run(self,
            printer: OutputStream,
            database: Optional[Database],
            constraints: Optional[Constraints],
            labels: Optional[Labels],
            parameters
            ):
        printer.print("My message on this line: ")
        printer.println("Hello there !")

        printer.clear()
        printer.println("I'm in the Matrix")
```
Some explanation about parameters. This first one is an `OutputStream` parameter which is an interface between
the module and text block where information are displayed. Three next parameters are `Database`, `Constraints` and 
`Labels` in this order.

!!! note "Optional Parameters"
    `Database`, `Constraints` and `Labels` are optional ! 
    It's a good practice to check before you code that required parameters are here !

!!! note "Initial display block status"
    Before to call a `GenericModule`, the text block is clear.

For example if you need to describe a database, it's could be a solution to create new `GenericModule`.
```python
from pyclusters.core.entities import DatabaseFactory, Database
from pyclusters.core.interface import DatabaseTransformerModule


class IdentityDatabaseTransformerModule(DatabaseTransformerModule):


    def transform(self, database: Database, parameters) -> Database:
        return DatabaseFactory.build_from_data(
            data = database.get_data(),
            headers = []
        )
```

## Parameters
Modules may need parameters either because it is a mandatory option required by the module code, or 
to customize the execution. 
In both case, parameters need to be declared before execution via `require() -> ParametersBag` method in your module.

In this way, when module is executed, all required parameters are given to it as a **map** 
where the **key is the parameter's name** and the **value is the parameter's value**.

Let's see an example with database visualizer module:
```python 
from matplotlib.figure import Figure
from pyclusters.core.entities import Database
from pyclusters.core.interface import DatabaseVisualizerModule, ParametersBag, Range, Choices

class MyVisualizerModule(DatabaseVisualizerModule):
    PARAM_COLUMN_1 = "Column 1"
    PARAM_COLUMN_2 = "Column 2"
    PARAM_MESSAGE = "Message"

    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add_range( MyVisualizerModule.PARAM_COLUMN_1, Range.build( min = 1 ) )
        parameters.add_range( MyVisualizerModule.PARAM_COLUMN_2, Range.build( min = 1 ) )
        parameters.add_range( MyVisualizerModule.PARAM_MESSAGE, Choices.build( 
            choices=[ "Hello", "Bye" ], 
            default_index=0 
        ) )
        return parameters

    def visualize_database(self, database: Database, parameters) -> Figure:
        
        # display message
        message = parameters[MyVisualizerModule.PARAM_MESSAGE]
        print(f"It's your message: {message}")
        
        # displays two dimensions
        colonne1 = int(parameters[MyVisualizerModule.PARAM_COLUMN_1])
        colonne2 = int(parameters[MyVisualizerModule.PARAM_COLUMN_2])
        
        data = database.get_data()

        f = Figure()
        a = f.add_subplot(111)
        a.scatter(data.transpose()[colonne1], data.transpose()[colonne2])

        return f
```

### ParametersBag
To manage parameters, `ParametersBag` object will contain all parameters required by your module.
Then `require()` function must return a filled `ParametersBag` object.

To create a new `ParametersBag` instance, use `ParametersBag.empty()`.
Then add a property use `add( name, property )` to add a property into `ParametersBag`.

Let's see an example:
```python
from pyclusters.core.interface import ParametersBag, Range, Choices, Confirmation, Entry

def require(self) -> ParametersBag:
    parameters = ParametersBag.empty()
    parameters.add("Range", Range.build(min=0, max=100, step=1))
    parameters.add("Choices", Choices.build(choices=["A", "B", "C"]))
    parameters.add("Confirmation", Confirmation.build(default=True))
    parameters.add("Entry", Entry.build(default="Hi there !"))
    return parameters
```

### Range
The `Range` object represents a range of value between a `min` and a `max`, with a given `step`.
To create a new Range, you must use `Range.build(min=MIN_VALUE, max=MAX_VALUE, step=1, default=None)`.
Note the presence of a `default` parameter which is quite important.
It's allows you to specify a default value and help user to define all parameters quickly.
If this parameter isn't given, the default value will be equals to `min`, `max` or 0 in this order.
Lastly, note `step` parameters which represent the step between possible value from another.
By default, this step is at 1.

!!! Warning 
    Currently, all values returned from range object are float !

```python
from pyclusters.core.interface import ParametersBag, Range

def require(self) -> ParametersBag:
    parameters = ParametersBag.empty()
    parameters.add("learning rate", Range.build(min=0, max=1, default=0.01, step=0.01))
    parameters.add("Nb iterations", Range.build(min=250, default=1000))
    return parameters
```
### Choices   
The `Choices` object represents a list of possible choices.
This parameter is quite simple, only composed with a list of `str` and a `default_index` initially at 0 
(representing the first value on the list). 
To create a new `Choices` object, you must use `Choices.build(choices, default_index=0)`.

```python
from pyclusters.core.interface import ParametersBag, Choices

def require(self) -> ParametersBag:
    parameters = ParametersBag.empty()
    parameters.add( "learning rate", Choices.build( choices=[ "Hello", "There", "!" ] ) )
    return parameters
```

### Entry
The `Entry` object represents a text field where user can write anything he wants.
To create a new one, you must use `Entry.build( default = "")` method.
If you want to specify a default value, you can write `Entry.build(default="Hi !")`.
By default, entry is empty.

```python
from pyclusters.core.interface import ParametersBag, Entry

def require(self) -> ParametersBag:
    parameters = ParametersBag.empty()
    parameters.add("Say hello", Entry.build(default="Hi !"))
    return parameters
```

### Confirmation
The `Confirmation` object represents a checkbox boolean field.
To create a new one, you must use `Confirmation.build( default = False )` method.
As other parameters, `default` parameters specify initial checkbox value.


```python
from pyclusters.core.interface import ParametersBag, Confirmation

def require(self) -> ParametersBag:
    parameters = ParametersBag.empty()
    parameters.add("Are you happy ?", Confirmation.build(default=True))
    return parameters
```


## Handling module exception
By default, all raised exceptions are caught during module execution, in order to prevent application crash.
This behavior is just a safety net and must be used for unexpected error.
When module execution cannot be done, prefer to raise `ModuleExecutionError` error.
The only one difference is on displayed error popup where a title and message can be specified
to fit error exactly. The idea behind this is to help user to understand why this error occured.

### Example
```py
class MyVisualizerModule(DatabaseVisualizerModule):
    
    def visualize_database(self, database: Database, parameters) -> Figure:
        raise ModuleExecutionError(
            title="Module Execution Error", 
            message="Please, execute this module with valid parameters"
        )
```




## Manage Module
The section details how to deal with modules.

PyClusters loads dynamically modules located under *PYCLUSTERS_LOCATION/modules/* when starting.
So on, once you create a module you may want to insert him here.

There are two ways to realize it. As modules location is just a directory, it's pretty 
easy to open it from *Files* and paste your fresh module at the right place.
Another cleaner solution is to use the CLI which aims easy module management.

!!! note "PyClusters location"
    To get PyClusters location, you can execute this command from your terminal: 

    `PYCLUSTERS_LOCATION=$(pip3 list | grep pyclusters | sed -e 's/ \+/ /g' | cut -d' ' -f 3)/pyclusters`

!!! note "Terminology"
    - A **Modules file** is a python which contains one or more modules.
    - A **Module class** or **Type** is the module abstraction, has defined in [Modules section](/modules/#modules)
    - A **Module** is a class which inherits from a module class. It is inside a Module File.
    
### Command Line Interface
Once PyClusters is installed in pip environment just enter command `python3 -m pyclusters` to check
that all works well.

```shell
$ python3 -m pyclusters
usage: __main__.py [-h] [-v] {start,listmod,testmod,pushmod,pullmod,delmod} ...

positional arguments:
  {start,listmod,testmod,pushmod,pullmod,delmod}
                        Sub-Parser help
    start               Start PyClusters application.
    listmod             List all modules registered in app.
    testmod             Test module to check integrity
    pushmod             Push given module file in PyClusters
    pullmod             Pull module from PyClusters to current or given location
    delmod              Delete module.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose
```

!!! warning
    testmod command is currently not supported.

#### start
Launch PyClusters application.

#### listmod
List all modules registered in application.
```shell
$ python3 -m pyclusters listmod -h
usage: __main__.py listmod [-h] [-t TYPE] [-m MODULE]

optional arguments:
  -h, --help            show this help message and exit
  -t TYPE, --type TYPE  Displays all module which inherit from module type only when type match with given pattern. For example, to display all modules inherited from IO
                        types, specify 'IO' as pattern.
  -m MODULE, --module MODULE
                        Displays module when it match with given pattern
```
This command is pretty useful to list registered module. For each module, you got his location.
```shell
$ python3 -m pyclusters listmod
Type LabelsIOModule:
        - MyLabelsIOModule on /io/textual.py
Type ConstraintsIOModule:
        - MyConstraintsIOModule on /io/textual.py
Type DatabaseIOModule:
        ...
```
For instance `- MyLabelsIOModule on /io/textual.py` means that `MyLabelsIOModule` is located 
at PYCLUSTERS_LOCATION/modules/**io/textual.py**.
```shell
$ PYCLUSTERS_LOCATION=$(pip3 list | grep pyclusters | sed -e 's/ \+/ /g' | cut -d' ' -f 3)/pyclusters
$ cat $PYCLUSTERS_LOCATION/modules/io/textual.py | grep -o MyLabelsIOModule
MyLabelsIOModule
```

It's possible to filter modules by type name.
```shell
# Searching by type name
$ python3 -m pyclusters listmod -t LabelsIOModule
Type LabelsIOModule:
        - MyLabelsIOModule on /io/textual.py
```
Or by module name directly.
```shell
# Searching by module name
$ python3 -m pyclusters listmod -m MyLabelsIOModule
Type LabelsIOModule:
        - MyLabelsIOModule on /io/textual.py
```


#### pushmod
Push given module file in PyClusters.

```shell
$ python3 -m pyclusters pushmod -h
usage: __main__.py pushmod [-h] [-f] [-l LOCATION] module

positional arguments:
  module                Specify the module push

optional arguments:
  -h, --help            show this help message and exit
  -f, --force           Force push module even if a file with same name already exists
  -l LOCATION, --location LOCATION
                        Specify module destination. For example, /path/to/module.py will locate module at {pyclusters_location}/modules/path/to/module.py
```

Let's take a pretty simple module example located in `test.py` on the current directory:
```python
from pyclusters.core.entities import Database, DatabaseFactory
from pyclusters.core.interface import  DatabaseIOModule

class MyTestDatabaseIOModule(DatabaseIOModule):

    def write(self, filename: str, database: Database):
        pass

    def read(self, filename: str) -> Database:
        return DatabaseFactory.build_from_data( [] )
```

To push it, just type this command on your terminal: 
```shell
$ python3 -m pyclusters pushmod test.py -l /io/test/test.py
Copy module from /home/lambda/test.py to /.../modules/io/test/test.py
```
Then, check that module has been well registered:
```shell
$ python3 -m pyclusters listmod -m MyTestDatabaseIOModule
Type DatabaseIOModule:
        - MyTestDatabaseIOModule on /io/test/test.py
```


#### pullmod
Pull module from PyClusters to current or given location.
```shell
$ python3 -m pyclusters pullmod -h
usage: __main__.py pullmod [-h] [-f] [-o FILENAME] module

positional arguments:
  module                Specify the module pull

optional arguments:
  -h, --help            show this help message and exit
  -f, --force           Force push module even if a file with same name already exists
  -o FILENAME, --filename FILENAME
                        Specify module filename.
```

Let's take our previous example after being registered.
Pulling our module file as `my_test.py`:
```shell
$ python3 -m pyclusters pullmod /io/test/test.py -o my_test.py
Pulling module
$ cat my_test.py
from pyclusters.core.entities import Database, DatabaseFactory
from pyclusters.core.interface import  DatabaseIOModule

class MyTestDatabaseIOModule(DatabaseIOModule):

    def write(self, filename: str, database: Database):
        pass

    def read(self, filename: str) -> Database:
        return DatabaseFactory.build_from_data( [] )
```

#### delmod
`delmod` command allows you to delete a module file. 
Let's take our previous example and delete it.

```shell
$ python3 -m pyclusters delmod /io/test/test.py
Module file /io/test/test.py will be deleted. Are you sure ? (Y/n) Y
```

## K-Means Module example

In this section, we will create a new `ClusteringAlgorithmModule` module which 
compute a clustering from database and constraints.

We will use the [COP-Kmeans Behrouz-Babaki's implementation](https://github.com/Behrouz-Babaki/COP-Kmeans) defined as 
a library.

### Module development
To create our new clustering algorithm module, we need to install dependencies.
As COP-KMeans has been developed as a library, the installation will be pretty simple:
```shell
$ git clone https://github.com/Behrouz-Babaki/COP-Kmeans.git
$ cd COP-Kmeans
$ python3 setup.py install # or pip3 install -e . to install copkmeans and keep sources locally.
```

We're ready to implement our module.
Let's create a new file called `copkmeans_algorithm.py` and write the following code:
```python
from pyclusters.core.entities import Database, Constraints, Labels, LabelsFactory
from pyclusters.core.interface import ClusteringAlgorithmModule, ParametersBag, Range
from pyclusters.exceptions.interface import ModuleExecutionError
from copkmeans.cop_kmeans import cop_kmeans



class COPKMeansClusteringAlgorithmModule(ClusteringAlgorithmModule):
    """COP-KMeans clustering algorithm module.

    This module uses COP-Kmeans Behrouz-Babaki's implementation available on github.
    """
    def require(self) -> ParametersBag:
        parameters = ParametersBag.empty()
        parameters.add("k", Range.build( min=1 ))
        return parameters

    def predict(self, database: Database, constraints: Constraints, parameters) -> Labels:
        # computes clusters
        # Warning: if clustering cannot be found due to constraints insatiability, clusters will be None
        clusters, center = cop_kmeans(
            dataset=database.get_data().tolist(),
            k=int(parameters["k"]),
            ml=constraints.get_ml().tolist(),
            cl=constraints.get_cl().tolist()
        )

        # So if no clustering found, raise appropriate exception
        if clusters is None:
            raise ModuleExecutionError(
                title="Clustering not found",
                message="No clustering which satisfies constraints exists."
            )


        return LabelsFactory.build_from_data( labels=clusters )
```

### Module integration
To integrate our new module in PyClusters, we will use the CLI to push it:
```shell
$ python3 -m pyclusters pushmod copkmeans_algorithm.py -l /algorithms/copkmeans.py
```

That's it ! `COPKMeansClusteringAlgorithmModule` is now available in PyClusters under `Algorithms` section.
Enjoy !



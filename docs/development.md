# Development

To contribute on PyClusters development, it's
a good idea to understand structure.

![](resources/pyclusters%20-%20structure.jpg)

## Entities
`Database`, `Labels` and `Constraints` are objects that we must be able to distinguish.
Moreover, these objects must have a name and a description.
Thus, we have defined `DatabaseEntity`, `LabelsEntity` and `ConstraintsEntity` used essentially in core and storage, 
which contain an id, a name, a description and more specific fields.


## Storage
As all application, we need to store data. 
All data are located under **~/.pyclusters** as follow:
```
~/.pyclusters
├── databases.yaml
├── projects.yaml
├── databases
│    └── <databases>
└── projects
    └── monpremierprojet
        ├── constraints
        │   └── <constraints files>
        ├── labels
        │   └── <labels files>
        └── views
            └── <views files>
```

To reduce used space disk all databases are project independent.
There are stored under `databases` directory. Each database's filename corresponds
to database's unique identifier. The file `databases.yaml` describes them in a yaml format.
In the same way, all projects are described in `projects.yaml` file and located under `project` directory.

A project is located inside `projects` directory under the name of his unique identifier.
It contains a `views` directory which contains project related views, `labels` directory for project related labels and
`constraints` directory for project related constraints. All files are saved under the name of their unique identifier.

The `StoreFacade` object performs all actions related with persistence.

## Core
When controllers need to perform an action which implies saving, deletion or accessing data, then they must 
called core object.


## Modules
Modules are loaded from `ModulesScanner` class. It produces new `ModulesRegister` instance which contains all modules 
loaded from modules package.


## User Interface
PyClusters have been developed with [Gtk+](https://python-gtk-3-tutorial.readthedocs.io/en/latest/).
To increase GUI development speed and quality, 
[Glade 3](https://python-gtk-3-tutorial.readthedocs.io/en/latest/builder.html) have been used.

### GtkController
GtkController is an abstraction of a controller which handles UI event in order to perform an action.
When you create a new controller, you must implement two functions:
- `init_view( object_accessor )` to initialize components that controller requires (e.g. entry, combo).
`object_accessor` is a function that can simply be called with an object's id. For instance, if you want to import
  an entry which has *entry-id* id, you must do `self.super_entry = object_accessor("entry_id)`.
  
- `start()` to start controller. This method is called each time that started content from which controller is related
starts.

### Gtk Toolbox
Gtk can be sometimes cumbersome and time-consuming. 
With this in mind, we have developed a toolkit in order to reduce both time and complexity by creating
specific manager. These classes are group in `interface.gtk_utils.py`.

### Gtk+ and Thread
When a button is clicked, Gtk executes a function in the UI thread.
A long process can block the user interface and leave the user that the application has crashed.
To avoid this, we use the `@OnThread` decorator which will allow the decorated function to be executed in a separate 
Thread. However, since Gtk is not Thread safe, operations that manipulate the interface (such as blocking a button, 
activating a spinner, etc.) must be done in the Thread UI. Thus, it is advisable to use the `@OnUIThread` decorator 
which will execute the decorated function in the Thread UI.


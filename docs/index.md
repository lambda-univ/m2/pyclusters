# PyClusters Documentation

Welcome to PyClusters documentation !

PyClusters is a desktop application designed in order to manipulate and visualize
constrained clustering. 
This application is developed in a research project from 
[Laboratoire d'Informatique Fondamentale d'Orléans](https://www.univ-orleans.fr/lifo/)
in [Contraintes & Apprentissages](https://www.univ-orleans.fr/lifo/equipe.php?id=1&lang=fr&sub=sub1) team
at [Orléans University](https://www.univ-orleans.fr/en).

## Theory
As PyClusters manipulates constrained clustering related object,
it's important to understand them. 
If these notions are not fully understood, you could read [Thoery page](/theory) page.

## Modules
An important PyClusters major feature is **modularity**. 
It's allow you to create new functionality by creating new module which can be inserted in PyClusters application
without extra modifications. 
Check [Modules page](/modules) to search how to define a new module and [Manage Module page](/manage-modules/)
to deal with them inside application.

## Contributing
Before to contribute, please read development page which relates information about PyClusters structure
fundamentals.

